#Written by Darian Hadjiabadi

#Program can take up to 3 additional command line arguments
import sys

def readWriteVotes(fileName):
    fileReader = open(fileName, 'r')
    fileWriter = open('processed-' + fileName, 'w')
    for line in fileReader:
        x = line.split(',')
        if (x[0] == "republican"):
            x[0] = 1
        else:
            x[0] = 0
        for i in range(len(x)):
            if (i < len(x) - 1):
                fileWriter.write(str(x[i]) + ',')
            else:
                fileWriter.write(str(x[i]))
    fileReader.close()
    fileWriter.close()

def readWriteMONK(fileName):
    fileReader = open(fileName, 'r')
    fileWriter = open('processed-' + fileName, 'w')
    for line in fileReader:
        x = line.split(' ')
        for i in range(len(x) - 2):
            if (i < len(x) - 3):
                fileWriter.write(str(x[i + 1]) + ',')
            else:
                fileWriter.write(str(x[i + 1]) + '\n')
    fileReader.close()
    fileWriter.close()

def readWriteMushroom(fileName):
    fileReader = open(fileName, 'r')
    fileWriter = open('processed-' + fileName, 'w')
    for line in fileReader:
        x = line.split(',')
        if (x[0] == 'e'):
            x[0] = 1
        else:
            x[0] = 0
        for i in range(len(x)):
            if (i < len(x) - 1):
                fileWriter.write(str(x[i]) + ',')
            else:
                fileWriter.write(str(x[i]))
    fileReader.close()
    fileWriter.close()
     
def main():

    argument = int(sys.argv[1])
    fileName = str(sys.argv[2])
    if (argument == 0): # will do processing for the votes data set
        readWriteVotes(fileName)
    elif (argument == 1): # will do processing for the MONK data set
        readWriteMONK(fileName)
        testFile = str(sys.argv[3]) # the test file
        readWriteMONK(testFile)
    elif (argument == 2): # will do processing for the mushroom data set
        readWriteMushroom(fileName)

main()

    
