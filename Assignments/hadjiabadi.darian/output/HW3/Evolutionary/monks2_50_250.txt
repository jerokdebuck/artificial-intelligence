Generation: 0
Average: 0.4361805
High: 0.52869797
Lowest: 0.32292905

Generation: 1
Average: 0.45405674
High: 0.53301805
Lowest: 0.39013827

Generation: 2
Average: 0.46770844
High: 0.5512451
Lowest: 0.2358578

Generation: 3
Average: 0.48150462
High: 0.5635331
Lowest: 0.40952754

Generation: 4
Average: 0.50110936
High: 0.5635331
Lowest: 0.41779193

Generation: 5
Average: 0.5223921
High: 0.5635331
Lowest: 0.3934836

Generation: 6
Average: 0.5272582
High: 0.56552434
Lowest: 0.4396264

Generation: 7
Average: 0.535545
High: 0.56552434
Lowest: 0.46501914

Generation: 8
Average: 0.5401762
High: 0.56552434
Lowest: 0.4384746

Generation: 9
Average: 0.54768735
High: 0.56552434
Lowest: 0.491652

Generation: 10
Average: 0.54789776
High: 0.56552434
Lowest: 0.49921158

Generation: 11
Average: 0.54550666
High: 0.56552434
Lowest: 0.4564585

Generation: 12
Average: 0.5487898
High: 0.56552434
Lowest: 0.50018513

Generation: 13
Average: 0.5518019
High: 0.56713957
Lowest: 0.49921158

Generation: 14
Average: 0.5537397
High: 0.56713957
Lowest: 0.5118088

Generation: 15
Average: 0.5534592
High: 0.56713957
Lowest: 0.5114218

Generation: 16
Average: 0.55446357
High: 0.5717744
Lowest: 0.51001066

Generation: 17
Average: 0.55512375
High: 0.5717744
Lowest: 0.5125787

Generation: 18
Average: 0.5573659
High: 0.5717744
Lowest: 0.51197016

Generation: 19
Average: 0.55407023
High: 0.5717744
Lowest: 0.5007697

Generation: 20
Average: 0.5570618
High: 0.5717744
Lowest: 0.50711405

Generation: 21
Average: 0.55317825
High: 0.5717744
Lowest: 0.49921158

Generation: 22
Average: 0.5545323
High: 0.5717744
Lowest: 0.49089614

Generation: 23
Average: 0.5548759
High: 0.5717744
Lowest: 0.50858486

Generation: 24
Average: 0.5573185
High: 0.5756276
Lowest: 0.50254357

Generation: 25
Average: 0.55704474
High: 0.5756276
Lowest: 0.4772539

Generation: 26
Average: 0.55831397
High: 0.59232235
Lowest: 0.51621825

Generation: 27
Average: 0.56238836
High: 0.59232235
Lowest: 0.49089614

Generation: 28
Average: 0.5672329
High: 0.59232235
Lowest: 0.51621825

Generation: 29
Average: 0.56891596
High: 0.5967307
Lowest: 0.5282844

Generation: 30
Average: 0.564924
High: 0.5967307
Lowest: 0.4772539

Generation: 31
Average: 0.5642925
High: 0.5967307
Lowest: 0.46835423

Generation: 32
Average: 0.57482034
High: 0.5967307
Lowest: 0.522567

Generation: 33
Average: 0.57295215
High: 0.60337734
Lowest: 0.5225715

Generation: 34
Average: 0.5691019
High: 0.60337734
Lowest: 0.4772539

Generation: 35
Average: 0.5755144
High: 0.60337734
Lowest: 0.5041551

Generation: 36
Average: 0.56695193
High: 0.60337734
Lowest: 0.4772539

Generation: 37
Average: 0.5745634
High: 0.60337734
Lowest: 0.4781189

Generation: 38
Average: 0.57429564
High: 0.60337734
Lowest: 0.4823063

Generation: 39
Average: 0.567877
High: 0.60337734
Lowest: 0.5282844

Generation: 40
Average: 0.56458396
High: 0.60337734
Lowest: 0.44943517

Generation: 41
Average: 0.56863976
High: 0.60337734
Lowest: 0.49370268

Generation: 42
Average: 0.5742
High: 0.60337734
Lowest: 0.42873457

Generation: 43
Average: 0.5708432
High: 0.60337734
Lowest: 0.4772539

Generation: 44
Average: 0.5783819
High: 0.60455984
Lowest: 0.4772539

Generation: 45
Average: 0.5799169
High: 0.60455984
Lowest: 0.48162362

Generation: 46
Average: 0.58127594
High: 0.6058899
Lowest: 0.5382779

Generation: 47
Average: 0.5799767
High: 0.60703033
Lowest: 0.51882344

Generation: 48
Average: 0.57982516
High: 0.60703033
Lowest: 0.5278283

Generation: 49
Average: 0.5809669
High: 0.6099689
Lowest: 0.5352093

Generation: 50
Average: 0.5813717
High: 0.61617875
Lowest: 0.5077057

Generation: 51
Average: 0.5849874
High: 0.61617875
Lowest: 0.53090805

Generation: 52
Average: 0.58600444
High: 0.61617875
Lowest: 0.5101953

Generation: 53
Average: 0.5827633
High: 0.61617875
Lowest: 0.4785344

Generation: 54
Average: 0.5835723
High: 0.6203857
Lowest: 0.4906686

Generation: 55
Average: 0.58950335
High: 0.6203857
Lowest: 0.5189891

Generation: 56
Average: 0.58924943
High: 0.6263289
Lowest: 0.5183707

Generation: 57
Average: 0.58630645
High: 0.6263289
Lowest: 0.5159681

Generation: 58
Average: 0.59538615
High: 0.6263289
Lowest: 0.5326022

Generation: 59
Average: 0.59283835
High: 0.6263289
Lowest: 0.51610565

Generation: 60
Average: 0.5898276
High: 0.6263289
Lowest: 0.48573634

Generation: 61
Average: 0.60361147
High: 0.6263289
Lowest: 0.56191546

Generation: 62
Average: 0.59755695
High: 0.6263289
Lowest: 0.52849

Generation: 63
Average: 0.5949769
High: 0.6263289
Lowest: 0.47979015

Generation: 64
Average: 0.5965442
High: 0.62889564
Lowest: 0.5367756

Generation: 65
Average: 0.60411096
High: 0.62889564
Lowest: 0.5627444

Generation: 66
Average: 0.599071
High: 0.62889564
Lowest: 0.48319376

Generation: 67
Average: 0.59404504
High: 0.62889564
Lowest: 0.49627414

Generation: 68
Average: 0.595931
High: 0.62889564
Lowest: 0.49519265

Generation: 69
Average: 0.5983177
High: 0.62889564
Lowest: 0.47286743

Generation: 70
Average: 0.59901214
High: 0.62889564
Lowest: 0.51924574

Generation: 71
Average: 0.60422754
High: 0.62889564
Lowest: 0.4849052

Generation: 72
Average: 0.6080349
High: 0.62889564
Lowest: 0.54772633

Generation: 73
Average: 0.6076688
High: 0.6320848
Lowest: 0.50612235

Generation: 74
Average: 0.61677206
High: 0.6320848
Lowest: 0.56927544

Generation: 75
Average: 0.61035806
High: 0.6320848
Lowest: 0.50612235

Generation: 76
Average: 0.60930586
High: 0.6320848
Lowest: 0.5380893

Generation: 77
Average: 0.60904616
High: 0.6320848
Lowest: 0.4911273

Generation: 78
Average: 0.6064198
High: 0.6320848
Lowest: 0.5521457

Generation: 79
Average: 0.60422784
High: 0.6320848
Lowest: 0.54624146

Generation: 80
Average: 0.60259485
High: 0.6320848
Lowest: 0.5034128

Generation: 81
Average: 0.5953783
High: 0.63843316
Lowest: 0.49972898

Generation: 82
Average: 0.6023556
High: 0.63843316
Lowest: 0.54463243

Generation: 83
Average: 0.59376997
High: 0.63843316
Lowest: 0.49972898

Generation: 84
Average: 0.5932932
High: 0.63843316
Lowest: 0.48079312

Generation: 85
Average: 0.5933549
High: 0.63843316
Lowest: 0.48514912

Generation: 86
Average: 0.5945883
High: 0.63843316
Lowest: 0.51084745

Generation: 87
Average: 0.58721673
High: 0.63843316
Lowest: 0.4682394

Generation: 88
Average: 0.5965492
High: 0.63843316
Lowest: 0.4807182

Generation: 89
Average: 0.5993823
High: 0.63843316
Lowest: 0.5233953

Generation: 90
Average: 0.5988432
High: 0.63843316
Lowest: 0.5021403

Generation: 91
Average: 0.6004369
High: 0.6385014
Lowest: 0.5004766

Generation: 92
Average: 0.60297346
High: 0.6385014
Lowest: 0.516185

Generation: 93
Average: 0.59962064
High: 0.6385014
Lowest: 0.50711405

Generation: 94
Average: 0.5981096
High: 0.6385014
Lowest: 0.516185

Generation: 95
Average: 0.59980845
High: 0.6385014
Lowest: 0.5251858

Generation: 96
Average: 0.6022744
High: 0.6385014
Lowest: 0.50844544

Generation: 97
Average: 0.601969
High: 0.6385014
Lowest: 0.50954187

Generation: 98
Average: 0.6127064
High: 0.6385014
Lowest: 0.5484183

Generation: 99
Average: 0.6088835
High: 0.6385014
Lowest: 0.516185

Generation: 100
Average: 0.6051182
High: 0.6385014
Lowest: 0.516185

Generation: 101
Average: 0.61278844
High: 0.63966215
Lowest: 0.55234337

Generation: 102
Average: 0.6104634
High: 0.63966215
Lowest: 0.5197509

Generation: 103
Average: 0.6110841
High: 0.64525163
Lowest: 0.53097767

Generation: 104
Average: 0.61479783
High: 0.64525163
Lowest: 0.54042923

Generation: 105
Average: 0.6135899
High: 0.64525163
Lowest: 0.5179222

Generation: 106
Average: 0.60679096
High: 0.64525163
Lowest: 0.46319377

Generation: 107
Average: 0.61664546
High: 0.64525163
Lowest: 0.5649216

Generation: 108
Average: 0.60428184
High: 0.64525163
Lowest: 0.5060524

Generation: 109
Average: 0.6068632
High: 0.64525163
Lowest: 0.5056702

Generation: 110
Average: 0.6053156
High: 0.64525163
Lowest: 0.48748478

Generation: 111
Average: 0.6054604
High: 0.64525163
Lowest: 0.49299145

Generation: 112
Average: 0.60582095
High: 0.64525163
Lowest: 0.53875273

Generation: 113
Average: 0.6042949
High: 0.64525163
Lowest: 0.49031758

Generation: 114
Average: 0.6020585
High: 0.64525163
Lowest: 0.5043398

Generation: 115
Average: 0.60117936
High: 0.64525163
Lowest: 0.47237438

Generation: 116
Average: 0.61292577
High: 0.64246297
Lowest: 0.54351217

Generation: 117
Average: 0.61350876
High: 0.64525163
Lowest: 0.5436444

Generation: 118
Average: 0.6047547
High: 0.64674383
Lowest: 0.5113562

Generation: 119
Average: 0.6062094
High: 0.64674383
Lowest: 0.5113562

Generation: 120
Average: 0.6072503
High: 0.64674383
Lowest: 0.523227

Generation: 121
Average: 0.6157582
High: 0.6476755
Lowest: 0.51997036

Generation: 122
Average: 0.6223684
High: 0.6476755
Lowest: 0.54511905

Generation: 123
Average: 0.6261598
High: 0.6476755
Lowest: 0.5593929

Generation: 124
Average: 0.6271161
High: 0.6501916
Lowest: 0.5756773

Generation: 125
Average: 0.62049437
High: 0.6501916
Lowest: 0.5623354

Generation: 126
Average: 0.6165628
High: 0.6501916
Lowest: 0.48174685

Generation: 127
Average: 0.6265788
High: 0.6538458
Lowest: 0.5798234

Generation: 128
Average: 0.61932284
High: 0.6538458
Lowest: 0.5564405

Generation: 129
Average: 0.6171976
High: 0.6538458
Lowest: 0.52879375

Generation: 130
Average: 0.6170884
High: 0.6538458
Lowest: 0.5119696

Generation: 131
Average: 0.6194672
High: 0.6504818
Lowest: 0.55315274

Generation: 132
Average: 0.61961406
High: 0.6504818
Lowest: 0.51107615

Generation: 133
Average: 0.62210155
High: 0.6504818
Lowest: 0.5260485

Generation: 134
Average: 0.61670935
High: 0.6504818
Lowest: 0.5338814

Generation: 135
Average: 0.61753994
High: 0.6504818
Lowest: 0.5009595

Generation: 136
Average: 0.6197612
High: 0.65697
Lowest: 0.525844

Generation: 137
Average: 0.6223013
High: 0.65697
Lowest: 0.51997036

Generation: 138
Average: 0.62041324
High: 0.65697
Lowest: 0.50978535

Generation: 139
Average: 0.61448073
High: 0.65697
Lowest: 0.5182177

Generation: 140
Average: 0.61956
High: 0.65697
Lowest: 0.51333696

Generation: 141
Average: 0.6226595
High: 0.6588291
Lowest: 0.55732787

Generation: 142
Average: 0.6223029
High: 0.6588291
Lowest: 0.525844

Generation: 143
Average: 0.62606406
High: 0.6588291
Lowest: 0.5642548

Generation: 144
Average: 0.6140579
High: 0.65697
Lowest: 0.5156146

Generation: 145
Average: 0.6245993
High: 0.65697
Lowest: 0.5540522

Generation: 146
Average: 0.619022
High: 0.6588291
Lowest: 0.53647393

Generation: 147
Average: 0.6230643
High: 0.6588291
Lowest: 0.53328675

Generation: 148
Average: 0.61761206
High: 0.6588291
Lowest: 0.5291479

Generation: 149
Average: 0.62800276
High: 0.6588291
Lowest: 0.5265523

Generation: 150
Average: 0.6261343
High: 0.6588291
Lowest: 0.5278077

Generation: 151
Average: 0.62454414
High: 0.6588291
Lowest: 0.53392935

Generation: 152
Average: 0.621394
High: 0.6588291
Lowest: 0.5319324

Generation: 153
Average: 0.6236508
High: 0.6588291
Lowest: 0.5382266

Generation: 154
Average: 0.63201696
High: 0.6588291
Lowest: 0.5819391

Generation: 155
Average: 0.6276578
High: 0.6588291
Lowest: 0.52688855

Generation: 156
Average: 0.627449
High: 0.6588291
Lowest: 0.56761086

Generation: 157
Average: 0.62061447
High: 0.6588291
Lowest: 0.5229187

Generation: 158
Average: 0.6202583
High: 0.6588291
Lowest: 0.4883647

Generation: 159
Average: 0.6261097
High: 0.6588291
Lowest: 0.51642513

Generation: 160
Average: 0.6315107
High: 0.6588291
Lowest: 0.5171393

Generation: 161
Average: 0.6282992
High: 0.6588291
Lowest: 0.55187887

Generation: 162
Average: 0.6233821
High: 0.6588291
Lowest: 0.51775825

Generation: 163
Average: 0.6253881
High: 0.66163194
Lowest: 0.53214705

Generation: 164
Average: 0.62488097
High: 0.66163194
Lowest: 0.5419525

Generation: 165
Average: 0.6265942
High: 0.66163194
Lowest: 0.54130864

Generation: 166
Average: 0.62022233
High: 0.66163194
Lowest: 0.5330567

Generation: 167
Average: 0.6196101
High: 0.66245437
Lowest: 0.5319407

Generation: 168
Average: 0.6261289
High: 0.66245437
Lowest: 0.55965614

Generation: 169
Average: 0.6198027
High: 0.66245437
Lowest: 0.51422805

Generation: 170
Average: 0.62964773
High: 0.66245437
Lowest: 0.5714073

Generation: 171
Average: 0.63216436
High: 0.66245437
Lowest: 0.57089305

Generation: 172
Average: 0.6161132
High: 0.66245437
Lowest: 0.52071357

Generation: 173
Average: 0.62308866
High: 0.6642386
Lowest: 0.5125333

Generation: 174
Average: 0.6284198
High: 0.6642386
Lowest: 0.54130864

Generation: 175
Average: 0.6281699
High: 0.6642386
Lowest: 0.528154

Generation: 176
Average: 0.6316913
High: 0.6642386
Lowest: 0.54337955

Generation: 177
Average: 0.6251669
High: 0.66962945
Lowest: 0.5398134

Generation: 178
Average: 0.6339032
High: 0.66962945
Lowest: 0.5235517

Generation: 179
Average: 0.6300242
High: 0.66962945
Lowest: 0.5281209

Generation: 180
Average: 0.62724245
High: 0.66962945
Lowest: 0.54967284

Generation: 181
Average: 0.63334626
High: 0.66962945
Lowest: 0.544

Generation: 182
Average: 0.62894326
High: 0.66962945
Lowest: 0.5260485

Generation: 183
Average: 0.63642645
High: 0.66962945
Lowest: 0.53605217

Generation: 184
Average: 0.6334498
High: 0.66962945
Lowest: 0.5548586

Generation: 185
Average: 0.63059384
High: 0.66962945
Lowest: 0.564342

Generation: 186
Average: 0.6293695
High: 0.67269015
Lowest: 0.55912244

Generation: 187
Average: 0.62293035
High: 0.67269015
Lowest: 0.5333904

Generation: 188
Average: 0.6239581
High: 0.67269015
Lowest: 0.5509854

Generation: 189
Average: 0.62490225
High: 0.67269015
Lowest: 0.5236008

Generation: 190
Average: 0.6341512
High: 0.67269015
Lowest: 0.54377437

Generation: 191
Average: 0.63964236
High: 0.67269015
Lowest: 0.58738595

Generation: 192
Average: 0.6314459
High: 0.67269015
Lowest: 0.5251102

Generation: 193
Average: 0.6267755
High: 0.67269015
Lowest: 0.5303101

Generation: 194
Average: 0.6241324
High: 0.67269015
Lowest: 0.5597391

Generation: 195
Average: 0.6231798
High: 0.67269015
Lowest: 0.52406645

Generation: 196
Average: 0.605753
High: 0.67269015
Lowest: 0.4842119

Generation: 197
Average: 0.6105775
High: 0.67269015
Lowest: 0.46906528

Generation: 198
Average: 0.62003756
High: 0.67269015
Lowest: 0.5417499

Generation: 199
Average: 0.62802786
High: 0.67269015
Lowest: 0.5684973

Generation: 200
Average: 0.62480694
High: 0.67269015
Lowest: 0.514053

Generation: 201
Average: 0.6342951
High: 0.67269015
Lowest: 0.5322168

Generation: 202
Average: 0.63711417
High: 0.67269015
Lowest: 0.53696275

Generation: 203
Average: 0.6384779
High: 0.67269015
Lowest: 0.56584746

Generation: 204
Average: 0.62853724
High: 0.67269015
Lowest: 0.5367854

Generation: 205
Average: 0.6398988
High: 0.67269015
Lowest: 0.56466824

Generation: 206
Average: 0.6249306
High: 0.67269015
Lowest: 0.51087177

Generation: 207
Average: 0.6344178
High: 0.67269015
Lowest: 0.53779775

Generation: 208
Average: 0.63953227
High: 0.67507887
Lowest: 0.5753924

Generation: 209
Average: 0.641674
High: 0.67507887
Lowest: 0.5835111

Generation: 210
Average: 0.64758927
High: 0.67507887
Lowest: 0.58737355

Generation: 211
Average: 0.6390465
High: 0.67507887
Lowest: 0.557942

Generation: 212
Average: 0.635301
High: 0.67507887
Lowest: 0.55899996

Generation: 213
Average: 0.6336592
High: 0.67507887
Lowest: 0.5477616

Generation: 214
Average: 0.633626
High: 0.6732557
Lowest: 0.55413926

Generation: 215
Average: 0.63613683
High: 0.6732557
Lowest: 0.49526006

Generation: 216
Average: 0.6420958
High: 0.6732557
Lowest: 0.5460293

Generation: 217
Average: 0.63600343
High: 0.67473304
Lowest: 0.53925073

Generation: 218
Average: 0.63461185
High: 0.67473304
Lowest: 0.5443938

Generation: 219
Average: 0.6324353
High: 0.67473304
Lowest: 0.5449713

Generation: 220
Average: 0.6354319
High: 0.67473304
Lowest: 0.5686446

Generation: 221
Average: 0.63883966
High: 0.67473304
Lowest: 0.5661354

Generation: 222
Average: 0.6340327
High: 0.67473304
Lowest: 0.5438696

Generation: 223
Average: 0.6306285
High: 0.67473304
Lowest: 0.56793606

Generation: 224
Average: 0.6350315
High: 0.67473304
Lowest: 0.56621444

Generation: 225
Average: 0.63211364
High: 0.67473304
Lowest: 0.55569756

Generation: 226
Average: 0.6394448
High: 0.67473304
Lowest: 0.5604377

Generation: 227
Average: 0.636573
High: 0.67655313
Lowest: 0.5407456

Generation: 228
Average: 0.63442856
High: 0.67655313
Lowest: 0.56123644

Generation: 229
Average: 0.6373361
High: 0.67655313
Lowest: 0.57314914

Generation: 230
Average: 0.63884526
High: 0.67416644
Lowest: 0.5644425

Generation: 231
Average: 0.6328017
High: 0.67416644
Lowest: 0.5449252

Generation: 232
Average: 0.6326406
High: 0.67416644
Lowest: 0.546824

Generation: 233
Average: 0.61773115
High: 0.67416644
Lowest: 0.5307743

Generation: 234
Average: 0.6234713
High: 0.67416644
Lowest: 0.52348274

Generation: 235
Average: 0.6236878
High: 0.66483915
Lowest: 0.5299268

Generation: 236
Average: 0.6335719
High: 0.66483915
Lowest: 0.5387107

Generation: 237
Average: 0.6282522
High: 0.66483915
Lowest: 0.5376234

Generation: 238
Average: 0.6354074
High: 0.66483915
Lowest: 0.58078647

Generation: 239
Average: 0.63768435
High: 0.66483915
Lowest: 0.5656198

Generation: 240
Average: 0.6316299
High: 0.6666238
Lowest: 0.54368675

Generation: 241
Average: 0.6284596
High: 0.6666238
Lowest: 0.5563581

Generation: 242
Average: 0.629906
High: 0.6666238
Lowest: 0.5308704

Generation: 243
Average: 0.63025284
High: 0.6666238
Lowest: 0.5362716

Generation: 244
Average: 0.63130033
High: 0.6666238
Lowest: 0.53738856

Generation: 245
Average: 0.63469434
High: 0.6666238
Lowest: 0.52973634

Generation: 246
Average: 0.64319575
High: 0.6666238
Lowest: 0.5855169

Generation: 247
Average: 0.622362
High: 0.6666238
Lowest: 0.5359339

Generation: 248
Average: 0.62366605
High: 0.6666238
Lowest: 0.5203433

Generation: 249
Average: 0.6314594
High: 0.66573066
Lowest: 0.5170891

Training took: 178.484412416 seconds
Accuracy: 0.576507
Precision: 0.4107143
Recall: 0.9484536
