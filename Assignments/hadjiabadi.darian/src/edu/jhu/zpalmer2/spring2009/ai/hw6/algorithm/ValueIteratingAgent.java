package edu.jhu.zpalmer2.spring2009.ai.hw6.algorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Collection;

import edu.jhu.zpalmer2.spring2009.ai.hw6.data.Action;
import edu.jhu.zpalmer2.spring2009.ai.hw6.data.State;
import edu.jhu.zpalmer2.spring2009.ai.hw6.data.WorldMap;
import edu.jhu.zpalmer2.spring2009.ai.hw6.util.DefaultValueHashMap;
import edu.jhu.zpalmer2.spring2009.ai.hw6.util.Pair;

public class ValueIteratingAgent implements ReinforcementLearningAgent
{
	private static final long serialVersionUID = 1L;
	
	/** A mapping between states in that world and their expected values. */
	private Map<State, Double> expectedValues;
	
	/** The world in which this agent is operating. */
	private WorldMap world;	
	/** The discount factor for this agent. */
	private double discountFactor;
	/** The transition function that this agent uses. */
	private TransitionFunction transitionFunction;
	/** The reward function that this agent uses. */
	private RewardFunction rewardFunction;
	/** The convergence tolerance (epsilon). */
	private double convergenceTolerance;

  
	
	/**
	 * Creates a new value iterating agent.
	 * @param world The world in which the agent will learn.
	 */
	public ValueIteratingAgent()
	{
		this.expectedValues = new DefaultValueHashMap<State, Double>(0.0);
		this.world = null;
		this.discountFactor = 0.5;
		this.transitionFunction = null;
		this.rewardFunction = null;
		this.convergenceTolerance = 0.000000001;
	}

	@Override
	public Policy getPolicy()
	{
		return new ValuePolicy();
	}


	/**
	 * Iterate performs a single update of the estimated utilities of each
	 * state.  Return value specifies whether a termination criterion has been
	 * met.
	 */
	@Override
	public boolean iterate()
	{
    // Set up initial mappings where each State will have utility equal to its reward
    // the reason it was not set to 0 is due to DefaultHashMap's source code. If a value is equal to the 
    // default value, it doesn't insert the key value pair. Therefore I just inserted the reward
    if (this.expectedValues.size() == 0) {
        Pair<Integer, Integer> trackDimensions = this.world.getSize();
        for (int dx = -5; dx <= 5; dx++) {
            for (int dy = -5; dy <= 5; dy++) {
                for (int i = 0; i < trackDimensions.getFirst(); i++) {
                    for (int j = 0; j < trackDimensions.getSecond(); j++) {
                        State newState = new State(new Pair<Integer, Integer>(i, j), new Pair<Integer, Integer>(dx, dy));
                        this.expectedValues.put(newState, this.rewardFunction.reward(newState));
                    }
                }
            } 
        }
    }
    Map<State, Double> prevIteration = new DefaultValueHashMap<State, Double>(0.0);
    prevIteration.putAll(this.expectedValues);
    
    //delta value
    double maxChange = 0.0;
    Set<Action> possibleActions = new Action(new Pair<Integer, Integer>(0,0)).LEGAL_ACTIONS;
      for (State s : prevIteration.keySet()) {
        double newUtil = this.rewardFunction.reward(s) + (this.discountFactor * this.argmax(possibleActions, s, prevIteration));
        this.expectedValues.put(s, newUtil);
        if (this.abs(prevIteration.get(s), newUtil) > maxChange) {
            maxChange = this.abs(newUtil, prevIteration.get(s));
        }
    }
    if (maxChange < (this.convergenceTolerance * (1.0 - this.discountFactor) / this.discountFactor)) {
        return true;
    }
    return false;
	}

  private double argmax(Set<Action> possibleActions, State currState, Map<State, Double> previous) {
      
      double max = -1000000.0;
      for (Action a : possibleActions) {
          Set<Pair<State, Double>> t = this.transitionFunction.transition(currState, a);
          double summation = 0.0;
          for (Pair<State, Double> p : t) {
              summation += (previous.get(p.getFirst()) * p.getSecond());
          }
          if (summation > max) {
              max = summation;
          }
        
      }
      //System.out.println(max);
      return max;
  }
            
  private double abs(double x, double y) {
      return (x - y < 0) ? (y - x) : (x - y);
  }
	public ValueIteratingAgent duplicate()
	{
		ValueIteratingAgent ret = new ValueIteratingAgent();
		ret.setConvergenceTolerance(this.convergenceTolerance);
		ret.setDiscountFactor(this.discountFactor);
		ret.setRewardFunction(this.rewardFunction);
		ret.setTransitionFunction(this.transitionFunction);
		ret.setWorld(this.world);
		ret.expectedValues.putAll(this.expectedValues);
		return ret;
	}
	
	public double getLearningFactor()
	{
		return discountFactor;
	}

	public void setDiscountFactor(double discountFactor)
	{
		this.discountFactor = discountFactor;
	}

	public TransitionFunction getTransitionFunction()
	{
		return transitionFunction;
	}

	public void setTransitionFunction(TransitionFunction transitionFunction)
	{
		this.transitionFunction = transitionFunction;
	}

	public RewardFunction getRewardFunction()
	{
		return rewardFunction;
	}

	public void setRewardFunction(RewardFunction rewardFunction)
	{
		this.rewardFunction = rewardFunction;
	}
	
	public WorldMap getWorld()
	{
		return world;
	}

	public void setWorld(WorldMap world)
	{
		this.world = world;
	}
	
	public double getConvergenceTolerance()
	{
		return convergenceTolerance;
	}

	public void setConvergenceTolerance(double convergenceTolerance)
	{
		this.convergenceTolerance = convergenceTolerance;
	}

	/**
	 * Represents a policy that this agent would produce.
	 */
	public class ValuePolicy implements Policy
	{
		private static final long serialVersionUID = 1L;
		
		private Random random = new Random();

		/**
		 * The action an agent decides to take from a given state 
		 */
		public Action decide(State state)
		{
        Set<Action> possibleActions = new Action(new Pair<Integer, Integer>(0,0)).LEGAL_ACTIONS;
        double max = -100000.0;
        ArrayList<Action> maxList = new ArrayList<Action>();
        //Action maxAction = null;
        for (Action a : possibleActions) {
            Set<Pair<State, Double>> t = transitionFunction.transition(state, a);
            double summation = 0.0;
            for (Pair<State, Double> p : t) {
                summation += (p.getSecond() * expectedValues.get(p.getFirst()));
            }
        /*    if (summation > max) {
                max = summation;
                maxAction = a;
            } */
            if (summation >= max) {
                if (summation == max) {
                    maxList.add(a);
                } else {
                    maxList.clear();
                    max = summation;
                    maxList.add(a);
                }
            }
        }
      return maxList.get(random.nextInt(maxList.size()));
      //  return maxAction;
		}
	}
}
