package edu.jhu.zpalmer2.spring2009.ai.hw6.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import edu.jhu.zpalmer2.spring2009.ai.hw6.data.Action;
import edu.jhu.zpalmer2.spring2009.ai.hw6.data.State;
import edu.jhu.zpalmer2.spring2009.ai.hw6.simulator.Simulator;
import edu.jhu.zpalmer2.spring2009.ai.hw6.simulator.SimulatorEvent;
import edu.jhu.zpalmer2.spring2009.ai.hw6.simulator.SimulationStep;
import edu.jhu.zpalmer2.spring2009.ai.hw6.simulator.SimulatorListener;
import edu.jhu.zpalmer2.spring2009.ai.hw6.util.DefaultValueHashMap;
import edu.jhu.zpalmer2.spring2009.ai.hw6.util.Pair;

/**
 * A reinforcement agent which uses the Q-learning technique.
 * 
 * @author Zachary Palmer
 */
public class QLearningAgent implements SimulationBasedReinforcementLearningAgent
{
	private static final long serialVersionUID = 1L;

	/** The number of times the agent will explore a given state-action pair before giving up on it. */
	private int minimumExplorationCount;
	/** The discount factor used by this agent to allow control over how important short-term gains are considered. */
	private double discountFactor;
	/** The learning factor for this agent. */
	private double learningFactor;
	/** The convergence tolerance (epsilon). */
	private double convergenceTolerance;

	/** Tracks the maximum change in our perception of the environment during an iteration. */
	double maximumChange = 0;

	/** The record of how frequently each action has been explored from each state. */
	private Map<Pair<State, Action>, Integer> visitEvents;
	/** The expected reward for the provided state-action pair. */
	private Map<Pair<State, Action>, Double> expectedReward;

	/** The simulator which is simulating the environment in which this agent is learning. */
	private transient Simulator simulator;

	/**
	 * General constructor.
	 */
	public QLearningAgent()
	{
		this.minimumExplorationCount = 1;
		this.discountFactor = 0.99;
		this.learningFactor = 0.5;
		this.convergenceTolerance = 0.000000001;

		this.visitEvents = new DefaultValueHashMap<Pair<State, Action>, Integer>(0);
		this.expectedReward = new DefaultValueHashMap<Pair<State, Action>, Double>(0.0);
		
		this.simulator = null;
	}

	@Override
	public Policy getPolicy()
	{
		return new QPolicy();
	}
	
	/**
	 * Iterates a single learn-as-I-go simulation for this Q learning agent. A
	 * single iteration of this algorithm will walk the agent to a goal state;
	 * thus, lower order iterations are likely to take much longer.  Return
	 * value specifies whether a termination criterion has been met.
	 */
	public boolean iterate()
	{
    // simulate under policy and check if max change is lower
    // than convergence
    this.simulator.simulate(new QPolicy());
    //System.out.println("Max change = " + this.maximumChange);
    double convergence = this.convergenceTolerance * (1.0 - this.discountFactor) / (this.discountFactor);
    if (this.maximumChange < convergence) {
        return true;
    }
    this.maximumChange = 0;
    return false;
	}
	
	@Override
	public Set<? extends SimulatorListener> getSimulatorListeners()
	{
		return Collections.singleton(new QLearningListener());
	}

	@Override
	public QLearningAgent duplicate()
	{
		QLearningAgent ret = new QLearningAgent();
		ret.setConvergenceTolerance(this.convergenceTolerance);
		ret.setDiscountFactor(this.discountFactor);
		ret.setLearningFactor(this.learningFactor);
		ret.setMinimumExplorationCount(this.minimumExplorationCount);
		ret.expectedReward.putAll(this.expectedReward);
		ret.visitEvents.putAll(this.visitEvents);
		return ret;
	}

	public int getMinimumExplorationCount()
	{
		return minimumExplorationCount;
	}

	public void setMinimumExplorationCount(int minimumExplorationCount)
	{
		this.minimumExplorationCount = minimumExplorationCount;
	}

	public double getDiscountFactor()
	{
		return discountFactor;
	}

	public void setDiscountFactor(double discountFactor)
	{
		this.discountFactor = discountFactor;
	}

	public double getLearningFactor()
	{
		return learningFactor;
	}

	public void setLearningFactor(double learningFactor)
	{
		this.learningFactor = learningFactor;
	}

	public double getConvergenceTolerance()
	{
		return convergenceTolerance;
	}

	public void setConvergenceTolerance(double convergenceTolerance)
	{
		this.convergenceTolerance = convergenceTolerance;
	}

	public Simulator getSimulator()
	{
		return simulator;
	}

	public void setSimulator(Simulator simulator)
	{
		this.simulator = simulator;
	}

	/**
	 * The policy used by this agent.
	 */
	class QPolicy implements Policy
	{
		private static final long serialVersionUID = 1L;
		
		/** A randomizer used to break ties. */
		private Random random = new Random();
		
		public QPolicy()
		{
			super();
		}

		@Override
		/**
		 * Returns the action the agent chooses to take for the given state.
		 */
		public Action decide(State state)
		{
      Set<Action> possibleActions = new Action(new Pair<Integer, Integer>(0,0)).LEGAL_ACTIONS;
      Action[] setActions = new Action[possibleActions.size()];
      possibleActions.toArray(setActions);
      if (possibleActions.size() == 0) {
          return null;
      }
      // if no action return null
      // find best action
      ArrayList<Action> maxActions = new ArrayList<Action>();
      double maxExpec = 0.0;
      for (int i = 0 ; i < setActions.length; i++) {
          if (i == 0) {
              maxExpec = expectedReward.get(new Pair<State, Action>(state, setActions[i]));
          }
          double explorationExpec = this.explorationFunction(setActions[i], state);
          if (explorationExpec >= maxExpec) {
              if (explorationExpec == maxExpec) {
                  maxActions.add(setActions[i]);
              } else {
                  maxActions.clear();
                  maxActions.add(setActions[i]);
                  maxExpec = explorationExpec;
              }
          }
      }
      return maxActions.get(random.nextInt(maxActions.size()));
	}
    private double explorationFunction(Action action, State state ) {
        // f(u,N) = R+ if n < threshold, else u
        final double optimalReward = 1.0;
        int freq = visitEvents.get(new Pair<State, Action>(state, action));
        if (freq >= minimumExplorationCount) {
            return expectedReward.get(new Pair<State, Action>(state, action));
        } else {
            return optimalReward;
        }
    }
  }
	/**
	 * The listener which learns on behalf of this agent.
	 */
	class QLearningListener implements SimulatorListener
	{
		/**
		 * Called once for every timestep of a simulation; every 
		 * time an agent takes an action, an "event" occurs.  
		 * Q-learning needs to do an update after every step, and this
		 * function is where it takes place.
		 */
		@Override
		public void simulationEventOccurred(SimulatorEvent event)
		{
      SimulationStep step = event.getStep();

      // get the 5 variables
      State prevState = step.getState();
      State newState = step.getResultState();
      Action action = step.getAction();

      double prevReward = step.getBeforeScore();
      double afterReward = step.getAfterScore();

      // indicates that prevState is terminal state
      if (newState == null) {
          expectedReward.put(new Pair<State, Action>(prevState, null), afterReward);
      }
      // else update
      if (prevState != null) {
           Pair<State, Action> p = new Pair<State, Action>(prevState, action);
          // increment count by 1
           int currFreq = visitEvents.get(p);
           visitEvents.put(p, currFreq + 1);
           this.update(prevReward, p, newState);

       }
		}
    private void update(double prevReward, Pair<State, Action> p, State newState) {
        Set<Action> possibleActions = p.getSecond().LEGAL_ACTIONS;
        double currentExpected = expectedReward.get(p);
        double newMaxReward = this.rewardArgMax(possibleActions, newState);
        //Q(s,a) = Q(s, a) + alpha * (r + gamma(maxQ(s',a)) - Q(s,a))
        double updatedReward = currentExpected + ( (learningFactor) * (prevReward + (discountFactor * newMaxReward) - currentExpected));
        if (this.abs(updatedReward, currentExpected) > maximumChange) {
            //System.out.println("Previous: " + currentExpected);
            //System.out.println("New max reward: " + newMaxReward);
            //System.out.println("Updated: " + updatedReward);
            //System.out.println();
            maximumChange = this.abs(updatedReward, currentExpected);
        }
        expectedReward.put(p, updatedReward);
    }
    private double abs(double x, double y) {
        return (x - y < 0) ? (y - x) : (x - y);
    }
    private double rewardArgMax(Set<Action> actions, State newState) {
        
        Action[] actionsArray = new Action[actions.size()];
        actions.toArray(actionsArray);
        double max = 0.0;
        for (int i = 0; i < actionsArray.length; i++) {
            if (i == 0) {
                max = expectedReward.get(new Pair<State, Action>(newState, actionsArray[i]));
            }
            double currExpec = expectedReward.get(new Pair<State, Action>(newState, actionsArray[i]));
            if (currExpec > max) {
                max = currExpec;
            }
        }
        return max;
    }
  }
}
