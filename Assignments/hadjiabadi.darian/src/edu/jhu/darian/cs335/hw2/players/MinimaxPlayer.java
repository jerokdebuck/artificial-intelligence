// Darian Hadjiabadi
// dhadjia1@gmail.com 
// dhadjia1

// since alpha beta pruning is a sub class of this algorithm,
// all comments in AlphaBetaPlayer.java are appreciable to here
// except for the pruning part

package edu.jhu.darian.cs335.hw2.players;

import java.util.ArrayList;
import edu.jhu.darian.cs335.hw2.players.Player;
import edu.jhu.darian.cs335.hw2.board.Board;
import edu.jhu.darian.cs335.hw2.board.Move;
import edu.jhu.darian.cs335.hw2.board.InvalidMoveException;
import edu.jhu.darian.cs335.hw2.board.Tile;
import edu.jhu.darian.cs335.hw2.board.Chip;
import edu.jhu.darian.cs335.hw2.datastructure.Node;

/** MiniMaxPlayer class for Konane game
  @author Darian Hadjiabadi
*/

public class MinimaxPlayer extends Player {
  int depth;
  final float upperBound = 1000;
  final float lowerBound = -1000;


  /** Create a MiniMaxPlayer
    @param int depth, the depth of the game
  */
  public MinimaxPlayer(int depth) {
      this.depth = depth;
  }

  /** Get the next moving using min-max algorithm
      @param Board game, the current state of the game
  */
  public Move getMove(Board game) {
      System.out.println("Turn " + game.getTurn() + ", legal moves(" + game.getLegalMoves().size() + "): ");
      for (Move printM : game.getLegalMoves()) {
          System.out.println(printM.toString());
      }
      Chip chip = Chip.NONE;
      if (game.isBlackUp()) {
          chip = Chip.BLACK;
      } else {
          chip = Chip.WHITE;
      }
      Node root = new Node(game, chip, 0);
      Node rootCopy = root.deepCopy(root);
      root = null;
      Node bestMove = null;
      bestMove = this.MaxMove(this.depth, rootCopy, chip);
      Move m = bestMove.getMove();
      System.out.println("Agent did the following move: " + m.toString());
      System.out.println("Expanded: " + bestMove.getExpansion()+ " nodes");
      m.insertExpanded(bestMove.getExpansion());
      return m;
  }
  private float getScore(Board game, boolean isBlack, int moveCountFromNode, int moveCountFromParent) {
      final int size = game.getSize();
      Tile[] data = game.getData();
      int friendlyPieces = 0;
      int enemyPieces = 0;
      for (int i = 0; i < data.length; i++) {
          if (data[i].getChip() == Chip.BLACK) {
              if (isBlack) {
                  friendlyPieces += 1;
              } else {
                  enemyPieces += 1;
              }
                      
          } else if (data[i].getChip() == Chip.WHITE) {
              if (!isBlack) {
                  friendlyPieces += 1;
              } else {
                  enemyPieces += 1;
              }
          }
      }
      // Min
      //if (this.depth % 2 == 0) {
          return  this.calculateEval(friendlyPieces, enemyPieces, moveCountFromParent, moveCountFromNode);
      // Max
     // } else { 
       //   return  -1 * this.calculateEval(friendlyPieces, enemyPieces, moveCountFromParent, moveCountFromNode);
     // }
  }
  float calculateEval(int friendlySum, int enemySum, int moveCountFromParent, int moveCountFromNode) {
      float moveFunction = (float) moveCountFromParent / (3 * (float) moveCountFromNode);
      if (this.depth % 2 == 1) {
          moveFunction *= -1;
      }
      float pieceFunction = (float) friendlySum / (3 * (float) enemySum);
      return (float) (.8 * moveFunction + .2 * pieceFunction);
  }

  float  evaluation(Node n, int moveCountFromParent, int moveCountFromNode) {
      Board game = n.getBoard();
      if (n.returnChip() == Chip.BLACK) { // scorebased off black pieces
          return this.getScore(game, true, moveCountFromParent, moveCountFromNode);
      } else {   // score based off white pices
          return this.getScore(game, false, moveCountFromParent, moveCountFromNode);
      }
  }
  private void checkState(Node n, Chip rootChip, int moveCountFromParent, int moveCountFromNode) {
      final float WINNER = 500;
      final float EVAL = this.evaluation(n, moveCountFromParent, moveCountFromNode);
      if (n.getBoard().gameWon() != Chip.NONE) { // win state
          n.insertUtility(WINNER);
      } else { // Depth level reached
          n.insertUtility(EVAL);
      } 
  }
  private boolean depthLimit(int deep) {
      return deep == 0;
  }
  private Node MaxMove(int deep, Node parent, Chip rootChip) {
      if (depthLimit(deep) || parent.getBoard().gameWon() != Chip.NONE) {
          this.checkState(parent, rootChip, parent.getParent().getBoard().getLegalMoves().size(), parent.getBoard().getLegalMoves().size());
            return parent;
      }
      double explored = parent.getExpansion();
      try {
          Node bestMove = null;
          float bestValue = lowerBound;
          ArrayList<Move> moves = parent.getBoard().getLegalMoves();
          Object[] moveArray = moves.toArray();
          Node p = null;
          Node curr = null;
          for (int i = 0; i < moveArray.length; i++) {
              explored += 1;
              p = parent.deepCopy(parent);
              Move m = (Move) moveArray[i];
              curr = new Node(m, parent, explored);
              if (parent.returnChip() == Chip.WHITE) {
                  curr.insertChip(Chip.BLACK);
              } else {
                  curr.insertChip(Chip.WHITE);
              }
              curr.Execute(p.getBoard());
              Node minNode = this.MinMove(deep - 1, curr, rootChip);
              if (deep != this.depth) {
                  minNode.changeMove(parent.getMove());
              }
              float minValue = minNode.getUtility();
              explored += minNode.getExpansion();
              if (minValue > bestValue) {
                  bestValue = minValue;
                  bestMove = minNode.deepCopy(minNode);
                  bestMove.insertUtility(minValue);
              }
          }
          return bestMove;
      } catch (InvalidMoveException ex) {
          System.err.println("ERROR AT MAX");
      }
      return null;
  }
  private Node MinMove(int deep, Node parent, Chip rootChip) { 
      if (depthLimit(deep) || parent.getBoard().gameWon() != Chip.NONE) {
          this.checkState(parent, rootChip, parent.getParent().getBoard().getLegalMoves().size(), parent.getBoard().getLegalMoves().size());
          return parent;
      }
      double explored = parent.getExpansion();
      try {
          Node bestMove = null;
          float bestValue = upperBound;
          ArrayList<Move> moves = parent.getBoard().getLegalMoves();
          Object[] moveArray =  moves.toArray();
          Node p = null;
          Node curr = null;
          for (int i = 0; i < moveArray.length; i++) {
              explored += 1;
              p = parent.deepCopy(parent);
              Move m = (Move) moveArray[i];
              curr = new Node(m, parent, explored);
              if (parent.returnChip() == Chip.BLACK) {
                  curr.insertChip(Chip.WHITE);
              } else {
                  curr.insertChip(Chip.BLACK);
              }
              curr.Execute(p.getBoard());
              Node maxNode = this.MaxMove(deep - 1, curr, rootChip);
              explored += maxNode.getExpansion();
              if (deep != this.depth) {
                  maxNode.changeMove(parent.getMove());
              }
              float maxValue = maxNode.getUtility();
              if (maxValue < bestValue) {
                  bestMove = maxNode.deepCopy(maxNode);
                  bestMove.insertUtility(maxValue);
                  bestValue = maxValue;
              }
          }
          return bestMove;
      } catch (InvalidMoveException ex) {
          System.err.println("ERROR AT MIN");
      }
      return null;
  }
  public String name() {
      return "MINIMAX";
  }
}
