package edu.jhu.darian.cs335.hw2;

import java.util.Scanner;
import java.util.Date;

import edu.jhu.darian.cs335.hw2.board.Board;
import edu.jhu.darian.cs335.hw2.board.Move;
import edu.jhu.darian.cs335.hw2.board.Chip;
import edu.jhu.darian.cs335.hw2.players.Player;
import edu.jhu.darian.cs335.hw2.players.HumanPlayer;
import edu.jhu.darian.cs335.hw2.players.MinimaxPlayer;
import edu.jhu.darian.cs335.hw2.players.AlphaBetaPlayer;
import edu.jhu.darian.cs335.hw2.players.RandomPlayer;
import java.io.PrintWriter;

/**
 * Driver class for Konane program; see README for description and terms of use
 * @author Ben Mitchell
 * Used by Darian Hadjiabadi for assignment 2 of Artificial Intelligence
 */
public class MultipleKonane {
  static PrintWriter file = null;

  public static void main(String[] args) {
    Board game;
    int boardSize;
    Player[] players = new Player[2];
    Scanner stdin = new Scanner(System.in);

      /* prompt user for game setup */
      System.out.println("Let's play Konane!");
      System.out.print("Enter board size: ");
      boardSize = stdin.nextInt();

      /* initialize game */
      players[0] = new AlphaBetaPlayer(4);
      players[1] = new RandomPlayer();
      int blackWin = 0; 
      int whiteWin = 0;
      try {
      for (int num = 0; num < 100; num++) {
          game = new Board(boardSize);
          while ( game.gameWon() == Chip.NONE ) {  
         /*   System.out.print("Turn " + game.getTurn() + ", ");
            if (game.getTurn()%2 == 0) {
              System.out.println("black to play:");
            } else {
              System.out.println("white to play:");
            }
            System.out.println(game);
*/
            long startTime = new Date().getTime();
            Move m = players[game.getTurn()%players.length].getMove(game);
            game.executeMove(m);
            long endTime = new Date().getTime();
            long duration = (endTime - startTime);
        //    System.out.println("Move took: " + duration + " milliseconds");
          }
          long endGame = new Date().getTime();
          System.out.println("Game over!");
          if (game.gameWon() == Chip.BLACK) {
              blackWin += 1;
          } else {
              whiteWin += 1;
          }
    }
    } catch (Exception e) {}
    System.out.println("BLACK HAS WON: " + blackWin);
    System.out.println("WHITE HAS WON: " + whiteWin);
}
}
