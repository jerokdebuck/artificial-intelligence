// Darian Hadjiabadi
// dhadjia1@gmail.com 
// dhadjia1

package edu.jhu.darian.cs335.hw2.players;

import java.util.ArrayList;
import edu.jhu.darian.cs335.hw2.players.Player;
import edu.jhu.darian.cs335.hw2.board.Board;
import edu.jhu.darian.cs335.hw2.board.Move;
import edu.jhu.darian.cs335.hw2.board.InvalidMoveException;
import edu.jhu.darian.cs335.hw2.board.Tile;
import edu.jhu.darian.cs335.hw2.board.Chip;
import edu.jhu.darian.cs335.hw2.datastructure.PruningNode;

/** AlphaBeta player class for Konane game
  *
  *
  *@author Darian Hadjiabadi
*/

public class AlphaBetaPlayer extends Player {
  int depth;
  final float lowerBound = -1000;
  final float upperBound = 1000;

  /** Insert the depth of the game
    @param int depth, the depth of the game
  */
  public AlphaBetaPlayer(int depth) {
      this.depth = depth;
  }

  /** Gets the next move using alpha beta pruning
    @param Board game, the current state of the game
  */
  public Move getMove(Board game) {
  //    System.out.println("Turn " + game.getTurn() + ", legal moves(" + game.getLegalMoves().size() + "): ");
    /*  for (Move printM : game.getLegalMoves()) {
          System.out.println(printM.toString());
      }*/
      Chip chip = Chip.NONE;
      if (game.isBlackUp()) {
          chip = Chip.BLACK;
      } else {
          chip = Chip.WHITE;
      }
      PruningNode root = new PruningNode(game, chip, 0);
      PruningNode rootCopy = root.deepCopy(root);
      root = null;
      PruningNode bestMove = null;
      int x = this.depth;
      // start min-max with pruning
      bestMove = this.MaxMove(x, rootCopy, chip);
      Move m = bestMove.getMove();
      m.insertExpanded(bestMove.getExpansion());
      //System.out.println("Agent did the following move: " + m.toString());
     // System.out.println("Expanded: " + bestMove.getExpansion()+ " nodes");
      return m;
  }
  private float getScore(Board game, boolean isBlack, int moveCountFromNode, int moveCountFromParent) {
      // iterate through data and obtain the friendly
      // and enemy piece count
      Tile[] data = game.getData();
      int friendlyPieces = 0;
      int enemyPieces = 0;
      for (int i = 0; i < data.length; i++) {
          if (data[i].getChip() == Chip.BLACK) {
              if (isBlack) {
                  friendlyPieces += 1;
              } else {
                  enemyPieces += 1;
              }
                      
          } else if (data[i].getChip() == Chip.WHITE) {
              if (!isBlack) {
                  friendlyPieces += 1;
              } else {
                  enemyPieces += 1;
              }
          }
      }
     // if (this.depth % 2 == 0) {
          return this.calculateEval(friendlyPieces, enemyPieces, moveCountFromParent, moveCountFromNode);
     // } else {
       //   return -1 * this.calculateEval(friendlyPieces, enemyPieces, moveCountFromParent, moveCountFromNode);
     // }
  }
  /* CALCULATE EVAL FUNCTION HERE */
  float calculateEval(int friendlySum, int enemySum, int moveCountFromParent, int moveCountFromNode) {
      // if f = moves from parent and g = moves from node
      // moveFunction = f / 3*g
      float moveFunction = (float) moveCountFromParent / (3 * (float) moveCountFromNode);
      if (this.depth % 2 == 1) { // from enemies point of view
          moveFunction *= -1;
      }
      // same idea here
      float pieceFunction = (float) friendlySum / (3 * (float) enemySum);
      return (float) (.20 * pieceFunction + .80 * moveFunction);
  }

  float evaluation(PruningNode n, int moveCountFromParent, int moveCountFromNode) {
      Board game = n.getBoard();
      if (n.returnChip() == Chip.BLACK) {
          return this.getScore(game, true, moveCountFromParent, moveCountFromNode);
      } else {
          return this.getScore(game, false, moveCountFromParent, moveCountFromNode);
      }
  }
  private void checkState(PruningNode n, Chip rootChip, int moveCountFromParent, int moveCountFromNode) {
      // give winner 500 points!
      final float WINNER = 500;
      final float EVAL = this.evaluation(n, moveCountFromParent, moveCountFromNode);
      if (n.getBoard().gameWon() != Chip.NONE) { // win state
          n.insertUtility(WINNER);
      } else { // Depth level reached
          n.insertUtility(EVAL);
      } 
  }
  private boolean depthLimit(int deep) {
      return deep == 0;
  }
  private PruningNode MaxMove(int deep, PruningNode parent, Chip rootChip) {
      // termination node
      if (depthLimit(deep) || parent.getBoard().gameWon() != Chip.NONE) {
          this.checkState(parent, rootChip, parent.getParent().getBoard().getLegalMoves().size(), parent.getBoard().getLegalMoves().size());
          parent.setTerminal();
          return parent;
      }
      int explored = parent.getExpansion();
      try {
          float bestValue = lowerBound;
          PruningNode bestMove = null;
          ArrayList<Move> moves = parent.getBoard().getLegalMoves();
          Object[] moveArray = moves.toArray();
          PruningNode p = null;
          PruningNode curr = null;
          for (int i = 0; i < moveArray.length; i++) {
              explored += 1;
              // do a deep copy of parent
              p = parent.deepCopy(parent);
              Move m = (Move) moveArray[i];
              curr = new PruningNode(m, parent, explored);
              if (parent.returnChip() == Chip.WHITE) {
                  curr.insertChip(Chip.BLACK);
              } else {
                  curr.insertChip(Chip.WHITE);
              }
              // execute move within the node
              curr.Execute(p.getBoard());
              PruningNode minNode = this.MinMove(deep - 1, curr, rootChip);
              float minValue = minNode.getUtility();
              // move the 'move' up
              if (deep != this.depth) {
                  minNode.changeMove(parent.getMove());
              }
              // check to see if we can prune or not
              if (minNode.isTerminal() && parent.getParent() != null && minValue >= parent.getParent().getBound()[1]) {
                  if (i == 0) {
                      bestMove = minNode.deepCopy(minNode);
                      bestMove.changeUpper(minValue);
                      bestMove.changeLower(minValue);
                      bestMove.insertUtility(minValue);
                      return bestMove;
                  } else {
                      bestMove.changeUpper(minValue);
                      bestMove.changeLower(minValue);
                      bestMove.insertUtility(minValue);
                      return bestMove;
                  }
              }
              explored += minNode.getExpansion();
            // check if new node beats the previous value
              if (minValue > bestValue) {
                  bestValue = minValue;
                  bestMove = minNode.deepCopy(minNode);
                  bestMove.insertUtility(minValue);
                  bestMove.changeLower(minValue);
              }
          }
          bestMove.changeUpper(bestValue);
          // update the bounds
          if (parent.getParent() != null) {
              if (parent.getParent().getBound()[1] >= bestValue) {
                  parent.getParent().changeUpper(bestValue);
              }
          }
          bestMove.setNotTerminal();
          return bestMove;
      } catch (InvalidMoveException ex) {
          System.err.println("ERROR AT MAX");
      }
      return null;
  }
  private PruningNode MinMove(int deep, PruningNode parent, Chip rootChip) {
      if (depthLimit(deep) || parent.getBoard().gameWon() != Chip.NONE) {
          this.checkState(parent, rootChip, parent.getParent().getBoard().getLegalMoves().size(), parent.getBoard().getLegalMoves().size());
          parent.setTerminal();
          return parent;
      }
      int explored = parent.getExpansion();
      try {
          PruningNode bestMove = null;
          float bestValue = upperBound;
          ArrayList<Move> moves = parent.getBoard().getLegalMoves();
          Object[] moveArray =  moves.toArray();
          PruningNode p = null;
          PruningNode curr = null;
          for (int i = 0; i < moveArray.length; i++) {
              explored += 1;
              p = parent.deepCopy(parent);
              Move m = (Move) moveArray[i];
              curr = new PruningNode(m, parent, explored);
              if (parent.returnChip() == Chip.BLACK) {
                  curr.insertChip(Chip.WHITE);
              } else {
                  curr.insertChip(Chip.BLACK);
              }
              curr.Execute(p.getBoard());
              PruningNode maxNode = this.MaxMove(deep - 1, curr, rootChip);
              float maxValue = maxNode.getUtility();
              if (deep != this.depth) {
                  maxNode.changeMove(parent.getMove());
              }
              if (maxNode.isTerminal() && parent.getParent() != null && maxValue <= parent.getParent().getBound()[0]) {
                  if (i == 0) {
                      bestMove = maxNode.deepCopy(maxNode);
                      bestMove.insertUtility(maxValue);
                      bestMove.changeUpper(maxValue);
                      bestMove.changeLower(maxValue);
                      return bestMove;
                  } else {
                      bestMove.insertUtility(maxValue);
                      bestMove.changeUpper(maxValue);
                      bestMove.changeLower(maxValue);
                      return bestMove;
                  }
              }
              explored += maxNode.getExpansion();
              if (maxValue < bestValue) {
                  bestMove = maxNode.deepCopy(maxNode);
                  bestMove.insertUtility(maxValue);
                  bestMove.changeUpper(maxValue);
                  bestValue = maxValue;
              }
          }
          bestMove.changeLower(bestValue);
          if (parent.getParent() != null) {
              if (parent.getParent().getBound()[0] <= bestValue) {
                  parent.getParent().changeLower(bestValue);
              }
          }
          bestMove.setNotTerminal();
          return bestMove;
      } catch (InvalidMoveException ex) {
          System.err.println("ERROR AT MIN");
      }
      return null;
  }
  public String name() {
      return "ALPHA BETA";
  }
}
