package edu.jhu.darian.cs335.hw2.players;

import java.util.ArrayList;
import java.util.Random;

import edu.jhu.darian.cs335.hw2.players.Player;
import edu.jhu.darian.cs335.hw2.board.Board;
import edu.jhu.darian.cs335.hw2.board.Move;
import edu.jhu.darian.cs335.hw2.board.InvalidMoveException;

public class RandomPlayer extends Player {

    public Move getMove(Board game) {
        Random rand = new Random();
        ArrayList<Move> moveList = game.getLegalMoves();
        System.out.println("Turn " + game.getTurn() + ", legal moves (" + moveList.size() + "): ");
        int size = moveList.size();
        int randy = rand.nextInt(size);
        return moveList.get(randy);
    }
}
        
