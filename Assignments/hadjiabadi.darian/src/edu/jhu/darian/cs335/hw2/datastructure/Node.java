// Darian Hadjiabadi
// dhadjia1@gmail.com 
// dhadjia1

package edu.jhu.darian.cs335.hw2.datastructure;
import edu.jhu.darian.cs335.hw2.board.Chip;
import edu.jhu.darian.cs335.hw2.board.Board;
import edu.jhu.darian.cs335.hw2.board.Move;
import edu.jhu.darian.cs335.hw2.board.InvalidMoveException;
import java.util.ArrayList;
import java.io.Serializable;
import java.io.*;

/** Node class for MiniMaxPlayer
  @author Darian Hadjiabadi
*/

public class Node implements Serializable {
    private static final long serialVersionUID = 7526472295622776147L;
    private float utility;
    private Chip player;
    private Board gameEncoding = null;
    private Move move = null;
    private double expanded;
    private Node parent = null;

    public Node(Board gameEncoding, Chip player, double explored) {
        this.gameEncoding = gameEncoding;
        this.player = player;
        this.expanded = explored;
    }
    public Node (Move m, Node parent, double explored) throws InvalidMoveException {
        this.parent = parent;
        this.move = m;
        this.expanded = explored;
    }
    public Node getParent() {
        return this.parent;
    }
    public double getExpansion() {
        return this.expanded;
    }
    public void inputExpansion(double expanded) {
        this.expanded = expanded;
    }
    public void insertChip(Chip chip) {
        this.player = player;
    }
    public Chip returnChip() { 
        return this.player;
    }
    public void insertUtility(float utility) {
        this.utility = utility;
    }
    public float getUtility() {
        return this.utility;
    }
    public Board getBoard() {
        return this.gameEncoding;
    }
    public void Execute(Board b) throws InvalidMoveException {
        this.gameEncoding = b;
        this.gameEncoding.executeMove(this.move);
    }
    public void insertBoard(Board b) {
        this.gameEncoding = b;
    }
    public Move getMove() {
        return this.move;
    }
    public void changeMove(Move m) {
        this.move = m;
    }
    public Node deepCopy(Node parent) { 
        try {
            ObjectOutputStream oos = null;
            ObjectInputStream ois = null;
            Node p = null;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(parent);
            oos.flush();
            ByteArrayInputStream bin = new ByteArrayInputStream(bos.toByteArray());
            ois = new ObjectInputStream(bin);
            Object o = ois.readObject();
            p = (Node) o;
            oos.close();
            ois.close();
            return p;
        } catch (ClassNotFoundException | IOException e) {
            System.err.println("COULD NOT DEEP COPY");
        }
        return null;
    }
}
        
