package edu.jhu.darian.cs335.hw2;

import java.util.Scanner;
import java.util.Date;

import edu.jhu.darian.cs335.hw2.board.Board;
import edu.jhu.darian.cs335.hw2.board.Move;
import edu.jhu.darian.cs335.hw2.board.Chip;
import edu.jhu.darian.cs335.hw2.players.Player;
import edu.jhu.darian.cs335.hw2.players.HumanPlayer;
import edu.jhu.darian.cs335.hw2.players.MinimaxPlayer;
import edu.jhu.darian.cs335.hw2.players.AlphaBetaPlayer;
import edu.jhu.darian.cs335.hw2.players.RandomPlayer;

/**
 * Driver class for Konane program; see README for description and terms of use
 * @author Ben Mitchell
 * Used by Darian Hadjiabadi for assignment 2 of Artificial Intelligence
 */
public class Konane {

  public static void main(String[] args) {
    Board game;
    int boardSize;
    Player[] players = new Player[2];
    Scanner stdin = new Scanner(System.in);

    try {

      /* prompt user for game setup */
      System.out.println("Let's play Konane!");
      System.out.print("Enter board size: ");
      boardSize = stdin.nextInt();

      /* initialize game */
      game = new Board(boardSize);
      String blackName = " ";
      String whiteName = " ";
      for (int i=0; i<2; i++) {
        System.out.print("Enter player type for ");
        if (i == 0)
          System.out.print("Black");
        else
          System.out.print("White");
        
        //System.out.print(" (1=human, 2=minmax, 3=alphabeta): ");
        System.out.print(" (1=human, 2=minmax, 3=alphabeta, 4 = random): ");
        int choice = stdin.nextInt();
        switch (choice) {
          case 1:
            players[i] = new HumanPlayer();
            break;
          case 2:
            System.out.print("Enter maximum search depth: ");
            int minmaxDepth = -1;
            while (minmaxDepth < 0) {
                minmaxDepth = stdin.nextInt();
                if (minmaxDepth < 0) {
                    System.out.println("Please enter depth value greater than 0 ");
                }
            }
            if (i == 0) {
                blackName = "MINIMAX";
            }
            if (i == 1) {
                whiteName = "MINIMAX";
            }
            players[i] = new MinimaxPlayer(minmaxDepth);
            break;
          case 3:
            System.out.println("Enter maximum search depth:" );
            int pruningDepth = -1;
            while (pruningDepth < 0) {
                pruningDepth = stdin.nextInt();
                if (pruningDepth < 0) { 
                    System.out.println("Please enter depth value greater than 0");
                }
            }
            players[i] = new AlphaBetaPlayer(pruningDepth);
            if (i == 0) {
                blackName = "ALPHABETA";
            }
            if (i == 1) {
                whiteName = "ALPHABETA";
            }
            break;
          case 4:
              players[i] = new RandomPlayer();
              break;
          default:
            System.out.println("bad agent type given, please try again...");
            i--;
        }
      }

      System.out.println("\n===================");

      /* take turns until gameover */
      long startGame = new Date().getTime();
      double blackExpanded = 0;
      double whiteExpanded = 0;
      while ( game.gameWon() == Chip.NONE ) {  
        System.out.print("Turn " + game.getTurn() + ", ");
        if (game.getTurn()%2 == 0) {
          System.out.println("black to play:");
        } else {
          System.out.println("white to play:");
        }

        System.out.println(game);

        long startTime = new Date().getTime();
        Move m = players[game.getTurn()%players.length].getMove(game);
        game.executeMove(m);
        if (game.getTurn()%2 == 1) {
            System.out.println(blackExpanded);
            blackExpanded += m.getExpanded();
        } else {
            System.out.println(whiteExpanded);
            whiteExpanded += m.getExpanded();
        }
        long endTime = new Date().getTime();
        long duration = (endTime - startTime);
        System.out.println("Move took: " + duration + " milliseconds");
      }
      long endGame = new Date().getTime();
      System.out.println("Game took: " + (endGame - startGame) + " milliseconds"); 
      System.out.println("Game over!  Final board state:\n" + game);
      System.out.println(blackName + " expanded total: " + blackExpanded + " nodes");
      System.out.println(whiteName + "  expanded total: " + whiteExpanded + " nodes");


      if (game.gameWon() == Chip.BLACK) {
        System.out.println("Game won by Black after " + game.getTurn() + " turns");
      } else {
        System.out.println("Game won by White after " + game.getTurn() + " turns");
      }


    } catch (Exception e) {
      System.err.println("Caught an exception: \n\t" + e.toString());
    } 

  } 

}
