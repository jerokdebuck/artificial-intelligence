// Darian Hadjiabadi
// dhadjia1@gmail.com 
// dhadjia1

package edu.jhu.darian.cs335.hw2.datastructure;
import edu.jhu.darian.cs335.hw2.board.Chip;
import edu.jhu.darian.cs335.hw2.board.Board;
import edu.jhu.darian.cs335.hw2.board.Move;
import edu.jhu.darian.cs335.hw2.board.InvalidMoveException;
import java.util.ArrayList;
import java.io.Serializable;
import java.io.*;

/** PruningNode class for AlphaBetaPlayer
  @author Darian Hadjiabadi
*/

public class PruningNode implements Serializable {
    private static final long serialVersionUID = 7526472295622776147L;
    private final float boundInit = 1000;

    private float utility;
    private Chip player;
    private Board gameEncoding = null;
    private Move move = null;
    private int expanded;
    private PruningNode parent;
    private float[] bounds = {-1 * boundInit, boundInit};
    boolean terminal = false;

    public PruningNode(Board gameEncoding, Chip player, int explored) {
        this.gameEncoding = gameEncoding;
        this.player = player;
        this.expanded = explored;
    }
    public PruningNode (Move m, PruningNode parent, int expanded) throws InvalidMoveException {
        this.move = m;
        this.parent = parent;
        this.expanded = expanded;
    }
    public boolean isTerminal() {
        return this.terminal;
    }
    public void setTerminal() {
        this.terminal = true;    
    }
    public void setNotTerminal() {
        this.terminal = false;
    }
    public PruningNode getParent() {
        return this.parent;
    }
    public int getExpansion() {
        return this.expanded;
    }
    public void inputExpansion(int expanded) {
        this.expanded = expanded;
    }
    public void insertChip(Chip chip) {
        this.player = player;
    }
    public Chip returnChip() { 
        return this.player;
    }
    public void insertUtility(float utility) {
        this.utility = utility;
    }
    public float getUtility() {
        return this.utility;
    }
    public Board getBoard() {
        return this.gameEncoding;
    }
    public void Execute(Board b) throws InvalidMoveException {
        this.gameEncoding = b;
        this.gameEncoding.executeMove(this.move);
    }
    public void insertBoard(Board b) {
        this.gameEncoding = b;
    }
    public Move getMove() {
        return this.move;
    }
    public float[] getBound() {
        return this.bounds;
    }
    public void changeBound(float[] bounds) {
        this.bounds = bounds;
    }
    public void changeLower(float lower) {
        this.bounds[0] = lower;
    }
    public void changeUpper(float upper) {
        this.bounds[1] = upper;
    }
    public void changeMove(Move m) {
        this.move = m;
    }
    public PruningNode deepCopy(PruningNode parent) { 
        try {
            ObjectOutputStream oos = null;
            ObjectInputStream ois = null;
            PruningNode p = null;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(parent);
            oos.flush();
            ByteArrayInputStream bin = new ByteArrayInputStream(bos.toByteArray());
            ois = new ObjectInputStream(bin);
            Object o = ois.readObject();
            p = (PruningNode) o;
            oos.close();
            ois.close();
            return p;
        } catch (ClassNotFoundException | IOException e) {
            System.err.println("COULD NOT DEEP COPY");
        }
        return null;
    }
}
        
