
package edu.jhu.darian.cs335.hw4.DataStructures;
import edu.jhu.darian.cs335.hw4.DataStructures.Node;
import java.io.*;

/**
    An edge class that supports connection between two Node instances
*/
public class Edge implements Serializable {

    private static final long serialVersionUID = 7526472295622776147L;
    private String label;
    private Node from;
    private Node to;

    /**
        Creates an Edge instance.
        @param from the parent node
        @param to the child node
        @param label the label with which to give the edge
    */
    public Edge(Node from, Node to, String label) {
        this.from = from;
        this.to = to;
        this.label = label;
    }
    /**
        Return the label of the edge
        @return String the label of the edge
    */
    public String getLabel() {
        return this.label;
    }
    /**
        Change the label of the edge
        @param label the new edge label
    */
    public void changeLabel(String label) {
        this.label = label;
    }
    /**
        Returns the Node with which the edge points to
        @return Node the child node
    */
    public Node getTo() {
        return this.to;
    }
    /**
        Return the Node which the edge points from.
        @return Node the parent node
    */
    public Node getFrom() {
        return this.from;
    }
    /**
        Change where the edge points to
        @param to the node with which edge now points to
    */
    public void changeTo(Node to) {
        this.to = to;
    }

}
