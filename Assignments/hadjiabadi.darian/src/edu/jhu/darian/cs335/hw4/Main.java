package edu.jhu.darian.cs335.hw4;
import edu.jhu.darian.cs335.hw4.DataStructures.DecisionTree;
import edu.jhu.darian.cs335.hw4.Classifier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
  
import java.util.Random;
import java.util.ArrayList;

/**
    Decision Trees.

    Main runner for Assignment 4 of Artificial Intelligence
    
    Allows up to 3 command line arguements
        1) boolean value: whether to run traditional or evolutionary
        2) Data set
        3) Train data set (if applicable). In this case, 2) is train data
*/

public final class Main {

    static final int INITIAL_SIZE = 10;
    static int vector_length = 0;
    static int data_length = 0;

    static double SPLIT_SIZE = 0.60;


/* ++++++++++++++++++++++++++++++++++++++++++++++
    TUNABLE PARAMETERS. See readme for ideal states */
    // true = use information gain; false = use gain ratio
    static final boolean useInformationGain = false;
    static final int POP_SIZE = 50;
    static final int GENERATIONS = 500;
    // 0 = fitness proportionate, 1 = rank order
    static final int selectionPolicy = 1;
    // 0 = complete replacement, 1 = elitism
    static final int replacementPolicy = 1;

    // user can tune these to his/her needs, but I tried to 
    // find a triplet that worked for all data sets
    static final float accuracy_weight = (float) .35;
    static final float precision_weight = (float) .35;
    static final float recall_weight = (float) .30;
/* ++++++++++++++++++++++++++++++++++++++++++++++ */

    static String[] grow(String[] fileStr) {
        // doubles the size of the array
        String[] bigger = new String[fileStr.length * 2];
        for (int i = 0; i < fileStr.length; i++) {
            bigger[i] = fileStr[i];
        }
        return bigger;
    }
    static String[] cutOff(String[] fileStr, int line_counter) {
        // cuts off array based on last position that is not null
        // this is to prevent null values from passsing into testing
        String[] cutStr = new String[line_counter];
        for (int i = 0; i < line_counter; i++) {
            cutStr[i] = fileStr[i];
        }
        return cutStr;
    }
    static void getVectorLength(String line) {
        // retrieves the dimensionality of the state space
        String[] splitLine = line.split(",");
        vector_length = splitLine.length - 1;
    }
    static String[] readFile(String fileName, String[] fileStr) {
        data_length = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String line = br.readLine();
            getVectorLength(line);
            while (line != null) {
                if (data_length == fileStr.length) {
                    fileStr = grow(fileStr);
                }
                 
                fileStr[data_length] = line;
                data_length += 1;
                line = br.readLine();
            }
            br.close();
        } catch(Exception e) {
            System.out.println(fileName);
            System.err.println("There is an error: " + e + "\n");
        }
        fileStr = cutOff(fileStr, data_length);
        return fileStr;
    }
    /*
    static ArrayList<ArrayList<String>> splitData(int train_length, int test_length, ArrayList<ArrayList<String>> dataSplit, String[] fileStr) {
        for (int i = 0 ; i < train_length; i++) {
            dataSplit.get(0).add(fileStr[i]);
        }
        for (int k = 0; k < test_length; k++) {
            dataSplit.get(1).add(fileStr[k]);
        }
        return dataSplit;
    } */
    static ArrayList<String> randomize(ArrayList<String> data) {
        Random rand = new Random();
        for (int i = data.size() - 1; i > 0; i--) {
            int index = rand.nextInt(i + 1);
            String a = data.get(index);
            data.add(index, data.get(i));
            data.add(i, a);
        }
        return data;
    }
    static ArrayList<ArrayList<String>> splitDataRandom(ArrayList<ArrayList<String>> dataSplit, String[] fileStr) {
        // Perform a Knuth shuffle
        Random rand = new Random();
        for (int i = fileStr.length - 1; i > 0; i--) {
            int index = rand.nextInt(i + 1);
            String a = fileStr[index];
            fileStr[index] = fileStr[i];
            fileStr[i] = a;
        }
        // split data into testing and train based on SPLIT_SIZE
        double x = (double) data_length * SPLIT_SIZE;
        int trainSize = (int) x;
        int testSize = data_length - trainSize;
        for (int i = 0; i < data_length; i++) {
            if (trainSize > 0) {
                dataSplit.get(0).add(fileStr[i]);
                trainSize -= 1;
            } else {
                dataSplit.get(1).add(fileStr[i]);
                testSize -= 1;
            }
        }
        return dataSplit;
    }
    /**
        Main method.
        @param args Command line arguments
        @throws FileNotFoundException if database(s) cannot be found
        @throws IOException if database file cannot be read properyl
    */

    public static void main(String[] args) {
        /* Make sure number of arguments is valid for program.
           Arguments should in format: <datafile> <boolean> OR
           <train file> <test file> <boolean>. In both cases, the boolean
           argument will determine whether to use information gain or
           gain ratio
        */
        if (args.length < 2 || args.length > 3) {
            throw new IllegalArgumentException("Incorrect number of arguments");
        }
        // if true, use evolutionary, else use traditional
        boolean useEvolutionary = Boolean.parseBoolean(args[0]);
        ArrayList<ArrayList<String>> dataSplit = new ArrayList<ArrayList<String>>();
        for (int t = 0; t < 2; t++) {
            dataSplit.add(new ArrayList<String>());
        }
        String directory = "../data/";
        String[] fileStr = new String[INITIAL_SIZE];
        fileStr = readFile(directory + args[1], fileStr);

        if (args.length == 2) {
            // Randomely split into train and test from an entire database
            splitDataRandom(dataSplit, fileStr);
        } else if (args.length == 3) {
            // retrieve testing information
            String[] testStr;
            for (int i = 0; i < fileStr.length; i++) {
                dataSplit.get(0).add(fileStr[i]);
            }
            testStr = new String[INITIAL_SIZE];
            testStr = readFile(directory + args[2], testStr);
            for (int j = 0; j < testStr.length; j++) {
                dataSplit.get(1).add(testStr[j]);
            }
        } 
        if (!useEvolutionary) {
            traditionalRun(dataSplit);
        } else {
            evolutionaryRun(dataSplit);
        }
    }
    static void traditionalRun(ArrayList<ArrayList<String>> dataSplit) {
        Classifier processor = new Classifier(dataSplit.get(0), vector_length, useInformationGain);
         float trainingInitial = System.nanoTime();
         DecisionTree dTree = processor.train();
         double trainTime = (System.nanoTime() - trainingInitial) * 1e-9;
         System.out.println("Training took: " + trainTime + " seconds");
         int[] results = processor.test(dataSplit.get(1), dTree); 
         //dTree.print();
         printResults(results);
    }
    static void evolutionaryRun(ArrayList<ArrayList<String>> dataSplit) {
        Classifier processor; 
      
        // split training data into trainingPopulation and developmentset
        ArrayList<String> trainingPopulation = dataSplit.get(0);
        ArrayList<String> developmentSet = new ArrayList<String>();
        ArrayList<String> testSet = new ArrayList<String>();
        ArrayList<DecisionTree> dTrees = new ArrayList<DecisionTree>();
        
        // split test data into test data and development data
        // where development will be used in the genetic algorithm
        // training and test data will be used to test the output
        // of the GA
        ArrayList<String> randomized = randomize(dataSplit.get(1));
        for (int j = 0; j < randomized.size(); j++) {
            if (j % 2 == 0) {
                developmentSet.add(randomized.get(j));
            } else {
                testSet.add(randomized.get(j));
            }
        }
        GA genetic = new GA(vector_length, developmentSet, trainingPopulation, accuracy_weight, recall_weight, precision_weight, POP_SIZE, GENERATIONS, replacementPolicy, selectionPolicy);
        float trainingInitial = System.nanoTime();
        DecisionTree maxTree = genetic.run();
        //maxTree.print();
        double trainTime = (System.nanoTime() - trainingInitial) * 1e-9;
        System.out.println("Training took: " + trainTime + " seconds");
        int[] results = new Classifier().test(testSet, maxTree);
        printResults(results);
        
    } 
    /*
        results[0] = true positives
        results[1] = true negatives
        results[2] = false positives
        results[3] = false negatives
    */

    static void printResults(int[] results) {
        System.out.println("Accuracy: " + ((float) (results[0] + results[1]) / (results[0] + results[1] + results[2] + results[3])));
        System.out.println("Precision: " + ((float) results[0] / (results[0] + results[2])));
        System.out.println("Recall: " + ((float) results[0] / (results[0] + results[3])));
       
    }
}

