package edu.jhu.darian.cs335.hw4.DataStructures;

import edu.jhu.darian.cs335.hw4.DataStructures.EntropyVectorTuple;
import edu.jhu.darian.cs335.hw4.DataStructures.Edge;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.io.*;

/** Node class for building a decision tree.
*/

public class Node implements Serializable {

    private static final long serialVersionUID = 7526472295622776147L;

    private String nodeLabel;
    private ArrayList<String> data;
    private ArrayList<Set<String>> attributePossibilities;
    private EntropyVectorTuple max;
    private int vector;
    private ArrayList<Edge> incomingEdges;
    private ArrayList<Edge> outgoingEdges;

    private boolean classLabel = false;

    /** Create a Node instance
        @param nodeLabel the label of the node
        @param data the current state of data
        @param attributePossibilities all remaining attribute possibilities
        @param max the maximum EntropyVectorTuple
        @param vector the vector_length
    */
    public Node(String nodeLabel, ArrayList<String> data, ArrayList<Set<String>> attributePossibilities, EntropyVectorTuple max, int vector) {
        this.data = data;
        this.nodeLabel = nodeLabel;
        this.attributePossibilities = attributePossibilities;
        this.max = max;
        this.vector = vector;
        this.outgoingEdges = new ArrayList<Edge>();
        this.incomingEdges = new ArrayList<Edge>();
    }
    /** Create a Node instance.
         @param classDecision the final decision
    */
    public Node(String classDecision) {
        this.nodeLabel = classDecision;
        this.classLabel = true;
        this.outgoingEdges = new ArrayList<Edge>();
        this.incomingEdges = new ArrayList<Edge>();
    }
    /** Return a boolean on whether or not the node is a leaf or decision node.
        @return boolean, true if leaf, false otherwise
    */
    public boolean isClass() {
        return this.classLabel;
    }
    public void makeLeaf() {
        this.outgoingEdges.clear();
    }
    /** Add an edge to the outgoing edge list.
         @param e the edge to add
    */
    public void addOutgoing(Edge e) {
        this.outgoingEdges.add(e);
    }
    /** Add an edge to the incoming edge list
        @param e the edge to add
    */
    public void addIncoming(Edge e) {
        this.incomingEdges.add(e);
    }
    /** Get the list of outgoing edges.
        @return ArrayList the list of outgoing edges
    */
    public ArrayList<Edge> getOutgoing() {
        return this.outgoingEdges;
    }
    /** Get the list of incoming edges
        @return ArrayList the list of incoming edges
    */
    public ArrayList<Edge> getIncoming() {
        return this.incomingEdges;
    }
    /** Alter the Nodes list of incoming edges
        @param incomingEdges the new list of incoming edges
    */
    public void changeIncoming(ArrayList<Edge> incomingEdges) {
        this.incomingEdges.clear();
        this.incomingEdges = incomingEdges;
    }
    /** Alter the Nodes list of outgoing edges
        @param outgoingEdges the new list of outgoing edges
    */
    public void changeOutgoing(ArrayList<Edge> outgoingEdges) {
        this.outgoingEdges = outgoingEdges;
    }
    /** Clear the node of all edges
    */
    public void clearEdges() {
        this.outgoingEdges.clear();
        clearIncoming();
    }
    /** Clear outgoing edges.
    */
    public void clearOutgoing(Edge e) {
        this.outgoingEdges.remove(e);
    }
    /** Clear incoming edges.
    */
    public void clearIncoming() {
        this.incomingEdges.clear();
    }
    /** Return the Node data variable
        @return ArrayList the data variable
    */
    public ArrayList<String> returnData() {
        return this.data;
    }
    /** Insert a new set of data into the Node
        @param data the new set of data
    */
    public void insertData(ArrayList<String> data) {
        this.data = data;
    }
    /** Return the max variable
        @return EntropyVectorTuple
    */
    public EntropyVectorTuple returnMax() {
        return this.max;
    }
    /** Return attributePossibilities variable.
        @return ArrayList the list of attribute possibilities
    */
    public ArrayList<Set<String>> returnAttPossibilities() {
        return this.attributePossibilities;
    }
    /** Return the vector variable
        @return int the vector variable
    */
    public int getLength() {
        return this.vector;
    }
    /** Return the Node's label
        @return String the Node's label
    */
    public String getLabel() {
        return this.nodeLabel;
    }
    /** Chagne the Node's label
        @param label the new label 
    */
    public void changeLabel(String label) {
        this.nodeLabel = label;
    }
    /** Make a deep copy of a node.
        @param parent the node to copy
        @return Node, the deep copy version of parent
    */
    public Node deepCopy(Node parent) {
        try {
            ObjectOutputStream oos = null;
            ObjectInputStream ois = null;
            Node p = null;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(parent);
            oos.flush();
            ByteArrayInputStream bin = new ByteArrayInputStream(bos.toByteArray());
            ois = new ObjectInputStream(bin);
            Object o = ois.readObject();
            p = (Node) o;
            oos.close();
            ois.close();
            return p;
        } catch (ClassNotFoundException | IOException e) {
            System.err.println("COULD NOT DEEP COPY");
        }
        return null;
    }
}
