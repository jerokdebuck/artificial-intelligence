package edu.jhu.darian.cs335.hw4;
import edu.jhu.darian.cs335.hw4.DataStructures.EntropyVectorTuple;
import edu.jhu.darian.cs335.hw4.DataStructures.DecisionTree;

import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
/**
    A class used to process a list of data in hopes
    of finding the maximum information gain or gain ratio
    metric from.
*/
public class DataProcessor {
   
    private ArrayList<String> data;
    private Set<String> classLabels;
    private ArrayList<String> classOccurances;
    private ArrayList<ArrayList<String>> attributeColumnVectors;
    private ArrayList<Set<String>> attributePossibilities;
    private ArrayList<Double> informationGain;
    private int vector;
    private double classEntropy;

    private boolean processed = false;
    private boolean useIG;
    /**
        Creates a DataProcessor instance.
        @param trainingData the data list from which to get infomration
        information from  
        @param vector_length the length of the attribute vector
    */
    public DataProcessor(ArrayList<String> trainingData, int vector_length, boolean useIG) {
        this.data = trainingData;
        this.useIG = useIG;
        this.classLabels = new HashSet<String>();
        this.classOccurances = new ArrayList<String>();
        this.attributeColumnVectors = new ArrayList<ArrayList<String>>();
        this.attributePossibilities = new ArrayList<Set<String>>();
        this.informationGain = new ArrayList<Double>();
        this.vector = vector_length;
    }
    /**
        Processes the training data and spits out the EntropyVectorTuple
        reprenting the highest information gain or gain ratio
        @return EntropyVectorTuple the instance containing highest
        information gain and position
    */
    public EntropyVectorTuple process() {
        this.getImportantInformation();
        if (this.classLabels.size() == 1) {
            return new EntropyVectorTuple(classLabels);
        }
        this.classEntropy = this.calculateClassEntropy();
        this.informationGain = this.calculateInformationGain();
        return this.getMax();

    }
    /**
        Return a list of attribute possibilities
        @return ArrayList, the arraylist
        containing a set of attribute possibilities
    */
    public ArrayList<Set<String>> getPossibilities() {
        return this.attributePossibilities;
    }
    /**
        Returns the maximum metric and its position
        in the form of an EntropyVectorTuple instance
        @return EntropyVectorTuple
    */
    private EntropyVectorTuple getMax() {
        double currMax = this.informationGain.get(0);
        int currMaxPos = 0;
        for (int i = 0; i < this.informationGain.size(); i++) {
            if (this.informationGain.get(i) > currMax) {
                currMax = this.informationGain.get(i);
                currMaxPos = i;
            }
        }
        return new EntropyVectorTuple(currMax, currMaxPos);
    }
    /**
        calculates the class entropy of a data list
        @return double, the class entropy
    */
    private double calculateClassEntropy() {
         int[] setSize = new int[classLabels.size()];
         String[] setArray = new String[setSize.length];
         classLabels.toArray(setArray);
        // decide on which class label was found and add
        // one to position in setSize that matches label
         for (int i = 0; i < classOccurances.size(); i++) {
            for (int j = 0; j < setArray.length; j++) {
                 if (setArray[j].compareTo(classOccurances.get(i)) == 0) {
                     setSize[j] += 1;
                 }
             }
         }
         return this.calculateEntropy(setSize);
    }    
    /**
        Returns the entropy from a given array of type int
        @param setSize, the input integer array
        @return double the entropy
    */
    private double calculateEntropy(int[] setSize) {
        /*
            Entropy = - sum [p(i) * log_{2}{p(i)}]
            where p(i) = |s(i)| / |s|
        */
        if (processed) {
            double ent = 0;
            int totalLength = 0;
            float[] probs = new float[setSize.length];
            for (int i = 0; i < setSize.length; i++) {
                totalLength += setSize[i];
                // calculated |s|
            }
            for (int j = 0; j < setSize.length; j++) {
                // calculate p(i)
                probs[j] = (float) setSize[j] / totalLength;
                // add to the current value of entropy
                // p(i) * log_{2}{p(i)}
                if (probs[j] != 0) {
                    ent += (probs[j] * (Math.log(probs[j]) / (Math.log(2))))    ;
                }
            }
            return -1 * ent;
          } else {
            return 0;
        }
         
    }
    /**
        Calculates the information metric useful for determining the
        next step in the decision tree process.
        @return ArrayList, a list of doubles containing the information
        gain (or gain ratio) of a vector of data
    */
    private ArrayList<Double> calculateInformationGain() {
       
        if (processed) {
            ArrayList<Double> informationGain = new ArrayList<Double>();
            ArrayList<Double> informationValue = new ArrayList<Double>();
            ArrayList<Double> conditionalEntropy = new ArrayList<Double>();
            int[] attribute_count = new int[vector];
            int class_count = classLabels.size();
            for (int i = 0; i < vector; i++) {
                attribute_count[i] = attributePossibilities.get(i).size();
            }
            // iterate over all attributes possibilites
            for (int j = 0; j < attribute_count.length; j++) {        
                String[] attributeLabels = new String[attribute_count[j]];
                attributePossibilities.get(j).toArray(attributeLabels);
                // returns an array of size #attribute types at
                // specific vector position
                int[] currentAttributeCount = this.calculateInformationValue(attribute_count[j], j, informationValue);
                int attTotal = 0;
                // find the total count
                for (int u = 0; u < currentAttributeCount.length; u++) {
                    attTotal += currentAttributeCount[u];
                }
                // get the column vector associated with j
                ArrayList<String> vectorColumn = attributeColumnVectors.get(j);
                // find conditional entropy H(s|a)
                // that is, given a specific class label, find all 
                // attributes for when label is present
                double conditional_ent = 0;
                for (int x = 0; x < currentAttributeCount.length; x++) {
                    int[] classAttCount = new int[classLabels.size()];
                    String attLabel = attributeLabels[x];
                    for (int k = 0; k < vectorColumn.size(); k++) {
                        String[] split = data.get(k).split(",");
                        int classLabel = Integer.parseInt(classOccurances.get(k));
                        if (split[j + 1].compareTo(attLabel) == 0) {
                            classAttCount[classLabel] += 1;
                        }

                    }
                    // H(s^j)
                    double classLabelEnt = this.calculateEntropy(classAttCount);
                    double setProb = (double) currentAttributeCount[x] / attTotal;
                    conditional_ent += classLabelEnt * setProb;
                    
                }
                conditionalEntropy.add(conditional_ent);
                
            }
            for (Double d : conditionalEntropy) {
                informationGain.add(classEntropy - d);
            }
            if (!this.useIG) {
                // return the gain ratio
                ArrayList<Double> gainRatio = new ArrayList<Double>();
                for (int p = 0; p < informationGain.size(); p++) {
                    Double gain = informationGain.get(p);
                    Double val = informationValue.get(p);
                    gainRatio.add(gain / val);
                }
                return gainRatio;
            }        
            return informationGain;

        } else {
            throw new IllegalArgumentException("Cannot find class entopy unless data has been processed");
        }
    }
    /**
        Calculates the inforamtion value of a given subset of data
        @return int[] the current attribute count
    */
    private int[] calculateInformationValue(int attribute_count, int loop_index, ArrayList<Double> informationValue) {
        String[] attributeLabels = new String[attribute_count];
        attributePossibilities.get(loop_index).toArray(attributeLabels);
        int[] currentAttributeCount = new int[attribute_count];
        // loop through column vector and increment array count by one
        // when an attribute label is seen
        for (String attLabel : attributeColumnVectors.get(loop_index)) {
            for (int k = 0; k < attribute_count; k++) {
                if (attLabel.compareTo(attributeLabels[k]) == 0) {
                    currentAttributeCount[k] += 1;
                }
            }
        }
        informationValue.add(this.calculateEntropy(currentAttributeCount));
        return currentAttributeCount;
    }
    /**
        Gets useful information froma  data list including class labels,
        attribute possibilties, a column list of attributes, etc
    */
    private void getImportantInformation() {
        /* This private function will get useful information from the 
           training data
        */
        for (int vec = 0; vec < vector; vec++) {
            attributeColumnVectors.add(new ArrayList<String>());
            attributePossibilities.add(new HashSet<String>());
        }
        for (int i = 0; i < data.size(); i++) {
            String[] splitLine = data.get(i).split(",");
            // add the class label to the list and the set
            // classLabels will be a set of all class labels in the data
            // while classOccurances will hold how many class labels were
            // found
            classLabels.add(splitLine[0]);
            classOccurances.add(splitLine[0]);
            for (int j = 1; j < splitLine.length; j++) {
                //essentially take data and transpose it
                attributeColumnVectors.get(j - 1).add(splitLine[j]);
                attributePossibilities.get(j - 1).add(splitLine[j]);
            }
        } 
        this.processed = true;
    }
    /**
        Returns the size of the information gain list
        @return int, the size of the gain list
    */
    public int gainSize() {
        return this.informationGain.size();
    }
}
