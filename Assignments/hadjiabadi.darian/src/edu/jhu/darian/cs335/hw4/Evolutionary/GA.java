package edu.jhu.darian.cs335.hw4;

import edu.jhu.darian.cs335.hw4.DataStructures.DecisionTree;
import edu.jhu.darian.cs335.hw4.Classifier;
import edu.jhu.darian.cs335.hw4.Evolutionary.Individual;

import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Random;
import java.io.*;

/** Genetic/Evolutionary algorithm for finding an optimal decision tree
*/

public class GA {


    private float crossOverRate = (float) 0.75;
    private float mutationRate = (float) 0.01;
    private final int subset_size = 30;

    private double bestAverage = 0;

    private Population bestGeneration;

    // initially a dummy
    private Individual bestIndividual = new Individual(null, 0);

    private int successive_failure = 0;
    private int generations = 0;

    private ArrayList<String> development;
    private ArrayList<String> training;
    private float accuracy_weight;
    private float recall_weight;
    private float precision_weight;

    private int POP_SIZE;
    private int GENERATIONS;
    //private int FAILURE = 100;
    private int replacementPolicy;
    private int selectionPolicy;
    private int vector_length;

    private Classifier dp;
    private Population currentPopulation;

    /** Class for containing a population of Individual instances
    */
    private class Population {
        private ArrayList<Individual> individuals;
        /** Create a population instance
        */
        public Population() {}
        /** Create a population instance
            @param individuals the ArrayList of individuals in the population
       */ 
        public Population(ArrayList<Individual> individuals) {
            this.individuals = individuals;
        }
        /** Order the individuals based off of fitness. Uses insertion sort
        */
        public void rankOrder() {
            int min;
            for (int i = 0; i < individuals.size() - 1; i++) {
                min = i;
                for (int j = i + 1; j < individuals.size(); j++) {
                    if (individuals.get(j).getFitness() < individuals.get(min).getFitness()) {
                        min = j;
                    }
                 }
                 if (min != i) {
                    float f = individuals.get(i).getFitness();
                    DecisionTree t = individuals.get(i).getEncoding();
                    
                    individuals.get(i).changeFitness(individuals.get(min).getFitness());
                    individuals.get(i).changeEncoding(individuals.get(min).getEncoding());
      
                    individuals.get(min).changeFitness(f);
                    individuals.get(min).changeEncoding(t);
                }
            } 
        } 
        /** Return the list of individuals in the population
            @return ArrayList the list of individuals
        */      
        public ArrayList<Individual> getPopulation() {
           return this.individuals;
        }
        /** Input a list of Individuals to be the new Population
            @param individuals the new population
        */
        public void input(ArrayList<Individual> individuals) {
            this.individuals = individuals;
        }
        /** Remove an individual from the population
            @param removal the individual to be removed
        */
        public void removeIndividual(Individual removal) {
            this.individuals.remove(removal);
        }
        /** Add an individual to the population
            @param toAdd the individual to add
        */
        public void addIndividual(Individual toAdd) {
            this.individuals.add(toAdd);
        }
        /** Retrieves the fitness individual in the population
            @return Individual the fittest individual
        */
        public Individual retrieveBest() {
            float max = 0;
            int index = 0;
            for (int i =0 ; i < this.individuals.size(); i++) {
                if (individuals.get(i).getFitness() > max) {
                    max = individuals.get(i).getFitness();
                    index = i;
                }
            }
            return this.individuals.get(index);
        }
        /** Gets the average fitness in the population
            @return float the average fitness
        */
        public float getFitnessAverage() {
            float sum = 0;
            for (Individual indy : this.individuals) {
                sum += indy.getFitness();
            }
            return sum / individuals.size();
        }
        /** Returns the fitness of the fittest individual
            @return float the fitness of the fittest
        */
        public float getHighest() {
            float max = 0;
            int index = 0;
            for (int i = 0 ; i < this.individuals.size(); i++) {
                if (individuals.get(i).getFitness() > max) {
                    max = individuals.get(i).getFitness();
                    index = i;
                }
            }
            return this.individuals.get(index).getFitness();
        }
        /** Returns the fitness of the weakest individual
            @return float the fitness of the weakest individual
        */
        public float getLowest() {
            float min = 10000;
            int index = 0;
            for (int i = 0 ; i < this.individuals.size(); i++) {
                if (individuals.get(i).getFitness() < min) {
                    min = individuals.get(i).getFitness();
                    index = i;
                }
            }
            return this.individuals.get(index).getFitness();
        }
        /** Returns the weakest individual
            @return Individual the weakest individual
        */  
        public Individual findWorst() {
            float min = 10000;
            int index = 0;
            for (int i = 0; i < this.individuals.size(); i++) {
                if (individuals.get(i).getFitness() < min) {
                    min = individuals.get(i).getFitness();
                    index = i;
                }
            }
            return this.individuals.get(index);
        }
    }
    /** Create a GA instance
        @param vector_length the length of the attribute vector
        @param development the development data set
        @param training the training data set
        @param accuracy_weight
        @param recall_weight
        @param precision_weight
        @param pop_size the size of the population
        @param generations the number of generations to run over
        @param replacementPolicy
        @param selectionPolicy
    */
    public GA(int vector_length, ArrayList<String> development, ArrayList<String> training, float accuracy_weight, float recall_weight, float precision_weight, int pop_size, int generations, int replacementPolicy, int selectionPolicy) {
        this.vector_length = vector_length;
        this.development = development;
        this.training = training;
        this.accuracy_weight = accuracy_weight;
        this.recall_weight = recall_weight;
        this.precision_weight = precision_weight;
        this.POP_SIZE = pop_size;
        this.GENERATIONS = generations;
        this.replacementPolicy = replacementPolicy;
        this.selectionPolicy = selectionPolicy;
   
        this.currentPopulation = new Population(); 
        this.dp = new Classifier();
    }
    private void shuffle() {
        Random rand = new Random();
        for (int i = training.size() - 1; i > 0; i--) {
            int r = rand.nextInt(i + 1);
            String a = training.get(r);
            training.add(r, training.get(i));
            training.add(i, a);
         }
         rand = null;
    }
        
    private Population generatePop(int generation_num) {
        // shuffle training data
        shuffle();
        ArrayList<Individual> individuals = new ArrayList<Individual>();
        Classifier processor;
        Random rand = new Random();
        Set<String> subset = new HashSet<String>();
        for (int i = 0; i < generation_num; i++) {
            // randomely choose points in training data
            // to fill set
            while (subset.size() != subset_size) {
                int r = rand.nextInt(training.size());      
                subset.add(training.get(r));
            }
            ArrayList<String> list = new ArrayList<String>();
            for (String str : subset) {
                list.add(str);
            }
            // make decision tree for data and get its fitness
            processor = new Classifier(list, vector_length, true);
            DecisionTree dTree = processor.train();
            processor = null;
            int[] results = dp.test(this.development, dTree);
            float fitness = this.evaluate(this.getAccuracy(results), this.getPrecision(results), this.getRecall(results));
            individuals.add(new Individual(dTree, fitness));
            subset.clear();
        }
        subset = null;
        rand = null;
        return new Population(individuals);
    }    
    private boolean terminationCheck() {
        // check to see if termination criteria has been met
        if (generations == GENERATIONS ) {
            return true;
        }
        return false;
    }
    /*
    private void updateBestGeneration() {
        this.bestGeneration = this.currentPopulation;
    }
    private void maxIndividual() {
        float max = 0;
        int counter = 0;
        ArrayList<Individual> indy = bestGeneration.getPopulation();
        for (int i = 0; i < indy.size(); i++) {
            if (indy.get(i).getFitness() > max) {
                max = indy.get(i).getFitness();
                counter = i;
            }
        }
        this.bestIndividual = indy.get(counter);
    } */
    /** Run the GA over the number of times as specified by pop_size
        @return DecisionTree the optimal decision tree
    */
    public DecisionTree run() {
        if (generations == 0) {
            this.currentPopulation = generatePop(this.POP_SIZE);
        }
        boolean termination = false;
        while (!termination) {
            // print some useful things here
            System.out.println("Generation: " + generations);
            float average = this.currentPopulation.getFitnessAverage();
            float high = this.currentPopulation.getHighest();
            float low = this.currentPopulation.getLowest();
            if (high > bestIndividual.getFitness() && replacementPolicy == 1) {
                Individual indy = this.currentPopulation.retrieveBest();
                this.bestIndividual = indy.deepCopy(indy);
                successive_failure = 0;
            } else {
                successive_failure += 1;
            }
            System.out.println("Average: " + average);
            System.out.println("High: " + high);
            System.out.println("Lowest: " + low);
            System.out.println();
            
            // use selection policy to get a new population
            Population midPop = new Population(getSelection());
            // do replacement based on replacement policy
            if (replacementPolicy == 0) {
                // if complete replacement, the current
                // population is just equal to midPop
                this.currentPopulation = midPop;
            } else {
                // else do elitism replacement
                this.currentPopulation = elitism(midPop); 
            }
            // incremenet generations by one and do a termination check
            generations += 1;
            termination = terminationCheck();
        }
        // return best individual in the current population
        return this.currentPopulation.retrieveBest().getEncoding();
    }
    // performs an elitist replacement policy
    private Population elitism(Population pop) {
        // if top individual seen so far is not present in current
        // population, then remove the weakest individual
        // for this top. else, keep population as it is
        ArrayList<Individual> indy = pop.getPopulation();
        boolean found = false;
        for (Individual i : indy) {
            if (i == bestIndividual) {
                found = true;
            }
        }
        if (!found) {
            pop.removeIndividual(pop.findWorst());
            pop.addIndividual(bestIndividual);
        }
        return pop;
    }
    // selections individuals to populate the next generation
    private ArrayList<Individual> getSelection() {
        ArrayList<Float> probabilityVector = new ArrayList<Float>();
        if (replacementPolicy == 0) {
            // uses fitness proportionate
            probabilityVector = this.calculateFitnessProportionate();
        } else {
            // uses rank order
            probabilityVector = this.calculateRankOrder();
        }
        // idea here is to select a parent from the probability "wheel"
        // if cross over, then select a second parent and produce an
        // offspring. Whether or not cross over occurs, do a mutation check
        // input into the new population the new individual, whether
        // crossover and/or mutation occured or not.
        ArrayList<Individual> selection = new ArrayList<Individual>();
        while (selection.size() != currentPopulation.getPopulation().size()) {
            // get the parent
            Individual xParent = this.getParent(probabilityVector);
            float chance = new Random().nextFloat();
            // check to see if cross over occurs
            if (chance < crossOverRate) {
                //System.out.println("I have crossed over");
                // get second parent and cross
                Individual yParent = this.getParent(probabilityVector);
                DecisionTree crossed = crossOver(xParent, yParent);
                //System.out.println("I am back from crossover");
                // get fitness of new decision tree
                int[] newResults = dp.test(this.development, crossed);
                float fitness = this.evaluate(this.getAccuracy(newResults), this.getPrecision(newResults), this.getRecall(newResults));
               // System.out.println(fitness);
                Individual child = new Individual(crossed, fitness);
                  // check to see if mutation should occur
                if (new Random().nextFloat() < mutationRate) {
                   // System.out.println("Mutated after xover");
                    Individual mutated = mutate(child);
                  //  System.out.println("Back from mutation");
                    selection.add(mutated);
                } else {
                    selection.add(child);
                }
               // selection.add(child);
             }
             else {
                // check to see if mutation should occur
                 if (new Random().nextFloat() < mutationRate) {
                    // System.out.println("Mutate");
                     Individual mutated = mutate(xParent);
                    //  System.out.println("Back from mutatin");
                     selection.add(mutated);
                 } else {
                     selection.add(xParent);
                 }
                // selection.add(xParent);
            }   
        }
        //System.out.println("I return thee");
        return selection;
        
    }
    private boolean check(DecisionTree dTree, ArrayList<DecisionTree> trees) {
        for (DecisionTree t : trees) {
            if (dTree == t) {
                return false;
            }
        }
            return true;
    }
    private ArrayList<Float> calculateRankOrder() {
        // Does rank order
        ArrayList<Float> probabilityVector = new ArrayList<Float>();
        this.currentPopulation.rankOrder();
        ArrayList<Individual> indy = this.currentPopulation.getPopulation();
        // weakest individual gets value 1 and increases by integer values
        float sum = 0;
        for (int i = 0; i < indy.size(); i++) {
            sum += (i + 1);
        }
        // individual will hold portion of probability wheel
        // based off rank. Since using rank order, it is not always
        // the case where an overly dominant individual will get chosen
        // this preserves the variety of the population
        float probability = 0;
        probabilityVector.add((float) 0);
        for (int i = 0; i < indy.size(); i++) {
            float prob = probability + ((i + 1) / sum);
            probabilityVector.add(prob);
            probability += ((i + 1) / sum);
        }
        return probabilityVector;
    }
        
    private ArrayList<Float> calculateFitnessProportionate() {
        ArrayList<Float> probabilityVector = new ArrayList<Float>();
        float sum = 0;
        ArrayList<Individual> indy = currentPopulation.getPopulation();
        for (Individual i : indy) {
            sum += i.getFitness();
        }
        // individual will get space on probability wheel that is
        // proportionate to its fitness
        float probability = 0;
        probabilityVector.add((float) 0);
        for (int i = 0; i < indy.size(); i++) {
             float prob = probability + (indy.get(i).getFitness() / sum);
             probabilityVector.add(prob);
             probability += (indy.get(i).getFitness() / sum);
        }
        return probabilityVector;
    }           
    private boolean feasibilityCheck(DecisionTree checkTree) {
        // check to make sure tree is logically correct
        int[] results = dp.test(this.development, checkTree);
        int sum = 0;
        for (int i =0 ; i < results.length; i++) {
            sum += results[i];
        }
        return sum != 0;
    }

    private Individual mutate(Individual indy) {
        // perform a mutation
        DecisionTree x = indy.getEncoding();
        DecisionTree xCopy;
        while (true) {
            xCopy = x.deepCopy(x);
            xCopy.mutate();
            if (feasibilityCheck(xCopy)) {
                int[] results = dp.test(this.development, xCopy);
                x = null;
                return new Individual(xCopy, this.evaluate(this.getAccuracy(results), this.getPrecision(results), this.getRecall(results)));
            } else {
                xCopy = null;
            }
        }
    }
    private DecisionTree crossOver(Individual xParent, Individual yParent) {
        // perform a crossover
        ArrayList<DecisionTree> returnTrees = new ArrayList<DecisionTree>();
        DecisionTree x = xParent.getEncoding();
        DecisionTree y = yParent.getEncoding();
        DecisionTree xCopy;
        DecisionTree yCopy;
        while (true) {
            xCopy = x.deepCopy(x);
            yCopy = y.deepCopy(y);
            xCopy.swapSubtree(yCopy);
            if (feasibilityCheck(xCopy)) {
                x = null;
                y = null;
                yCopy = null;
                return xCopy;
            } else {
                xCopy = null;
                yCopy = null;
            }
        }
    }
    private Individual getParent(ArrayList<Float> probabilityVector) {
        // use probability wheel to get a parent
        Random rand = new Random();
        float random = rand.nextFloat();
        while (true) {
            for (int j = 0; j < currentPopulation.getPopulation().size(); j++) {
                if (random > probabilityVector.get(j) && random < probabilityVector.get(j + 1)) {
                          rand = null;
                          return this.currentPopulation.getPopulation().get(j);
                }
            } 
        }
    }
    
    private float evaluate(float accuracy, float precision, float recall) {
        // get the fitness
      
        return ( (accuracy_weight * accuracy) + (recall_weight * recall) + (precision_weight * precision) );
    }
    private float getAccuracy(int[] results) {
        if (results[0] == 0 && results[1] == 0 && results[2] == 0 && results[3] == 0) {
            return (float) 0;
        }
        return ( ( (float) results[0] + results[1]) / (results[0] + results[1] + results[2] + results[3]));
    }
    private float getPrecision(int[] results) {
        if (results[0] == 0 && results[2] == 0) {
            return (float) 0;
        }
        return ( (float) results[0] / (results[0] + results[2]));
    }
    private float getRecall(int[] results) {
        if (results[0] == 0 && results[3] == 0) {
            return (float) 0;
        }
        return ( (float) results[0] / (results[0] + results[3]));
    }

}
