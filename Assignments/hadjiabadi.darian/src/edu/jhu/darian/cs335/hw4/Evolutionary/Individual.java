package edu.jhu.darian.cs335.hw4.Evolutionary;
import edu.jhu.darian.cs335.hw4.DataStructures.DecisionTree;
import java.io.*;

 /** Class for containing an Individual in a population. Keeps track
     of an individuals tree encoding and the fitness of the encoding.
     Had to be a public class in order to be deep copied (blame Java)
 */ 
 public class Individual implements Serializable {
        private DecisionTree encoding;
        private float fitness;
        static final long serialVersionUID = 7526472295622776147L;
        /** Create an Individual instance
            @param encoding the tree encoding
            @param fitness the fitness of the encoding
        */
        public Individual(DecisionTree encoding, float fitness) {
            this.encoding = encoding;
            this.fitness = fitness;
        }
        /** Return the fitness of the individual
            @return float the Individuals's fitness
        */
        public float getFitness() {
            return this.fitness;
        }
        /** Return the tree encoding of the individual
            @return DecisionTree the encoding
        */
        public DecisionTree getEncoding() {
            return this.encoding;
        }
        /** Change the encoding of the individual
            @param encoding the new encoding
        */
        public void changeEncoding(DecisionTree encoding) {
            this.encoding = encoding;
        }
        /** Change the fitness of the individual
            @param fitness the new fitness
        */
        public void changeFitness(float fitness) {
            this.fitness = fitness;
        }
        /** Make a deep copy of the individual
            @param parent the instance to copy
            @return Individual the deep copy version of parent
        */
        public Individual deepCopy(Individual parent) {
            try {
                ObjectOutputStream oos = null;
                ObjectInputStream ois = null;
                Individual p = null;
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                oos = new ObjectOutputStream(bos);
                oos.writeObject(parent);
                oos.flush();
                ByteArrayInputStream bin = new ByteArrayInputStream(bos.toByteArray());
                ois = new ObjectInputStream(bin);
                Object o = ois.readObject();
                p = (Individual) o;
                oos.close();
                ois.close();
                return p;
            } catch (ClassNotFoundException | IOException e) {
                System.err.println("COULD NOT DEEP COPY");
            }
            return null;
        }
    }

