package edu.jhu.darian.cs335.hw4.DataStructures;
import edu.jhu.darian.cs335.hw4.DataStructures.Node;
import edu.jhu.darian.cs335.hw4.DataStructures.Edge;

import java.util.Set;
import java.util.ArrayList;
import java.util.HashSet;
import java.io.*;
import java.util.Queue;
import java.util.LinkedList;
import java.util.Random;

/** DecisionTree class that contains a set of nodes and edges,
    forming a tree structure
*/

public class DecisionTree implements Serializable {

    private static final long serialVersionUID = 7526472295622776147L;
    
    // to be used for printing and finding a node
    private Queue<Node> q;
    private Node root;
    private int size;

    /** Return the size of the tree.
        @return int the size of the tree
    */
    public int getSize() {
        return this.size;
    }
    /** Insert a root node into the tree.
        @param root the root of the tree 
    */
    public void insertRoot(Node root) {
        this.root = root;
        this.size = 0;
    }
    /** Return the root of the tree
        @return Node the root
    */
    public Node getRoot() {
        if (this.root != null) {
            return this.root;
        }
        else {
            return null;
        }
    }
    /** Perform a mutation on a tree node
    */
    public void mutate() {
        Random rand = new Random();
        Node n = chooseNode();
        // if n contains a class label, randomely switch to another
        // label. It may be the case the label does not change
        if (n.isClass()) {
            if (rand.nextFloat() > 0.5) {
                n.changeLabel("1");
            } else {
                n.changeLabel("0");
            }
        // else sitch the label of a decision edge to another label
        } else {
            ArrayList<Edge> outgoing = n.getOutgoing();
            ArrayList<String> outgoingLabels = new ArrayList<String>(); 
            for (Edge e : outgoing) {
                outgoingLabels.add(e.getLabel());
            }
            // randomely choose a decision edge
            int x = rand.nextInt(outgoingLabels.size());
            Edge chosen = outgoing.get(x);
            x = rand.nextInt(outgoingLabels.size());
            // find another decision edge and give it that label
            String toChange = outgoing.get(x).getLabel(); 
            chosen.changeLabel(toChange);
        }
    }
    /** Swap subtrees with another DecisionTree instance
        @param other the DecisionTree whos subtree will be swapped with
    */
    public void swapSubtree(DecisionTree other) {
        // choose a pair of nodes, one from each tree
        Node n1 = chooseNode();
        Node n2 = other.chooseNode();
        
        // get n1's incoming edge (should only be one ayways since tree)
        // and swap with n2
        ArrayList<Edge> n1In = n1.getIncoming();
        for (Edge e : n1In) {
            if (e.getTo() == n1) {
                // change the node edge points to to n2
                e.changeTo(n2);
                // change n2's incoming edge list to n1In
                n2.changeIncoming(n1In);
                break;
            }
        }
    }
    /** Randomely chooses a node based off a uniform distribution.
        @return Node the chosen node
    */
    public Node chooseNode() {
        Random rand = new Random();
        int currCount = -1;
        Node currNode = this.root;
        q = new LinkedList<Node>();
        q.add(this.root);
        float initial = System.nanoTime();
        while (q.peek() != null) {
            // if search is taking too long then return currNode
            if ( ((System.nanoTime() - (double) initial) * 1e-9) > .3) {
                return currNode;
            }
            Node curr = q.remove();
            // with each level, there is a 1 / level chance of picking
            // the node. This is if we consider the root to be level 0
            currCount += 1;
            if (currCount > 0) {
                float prob = 1 / (float) currCount;
                float ran = rand.nextFloat();
                if (ran < prob) {
                    currNode = curr;
                }
            } 
            ArrayList<Edge> outgoing = curr.getOutgoing();
            //for (Edge e : outgoing) {
            // randomely decide which node to add to the queue
            if (outgoing.size() > 0) {
                int x = rand.nextInt(outgoing.size());
                q.add(outgoing.get(x).getTo());
                //q.add(e.getTo());
            }
        }
        q = null;
        return currNode;
    }
    /** Uses a BFS search to print the tree.
    */
    public void print() {
        Node n = this.root;
        q = new LinkedList<Node>();
        q.add(this.root);
        while (q.peek() != null) {
            Node curr = q.remove();
            System.out.println(curr);
            ArrayList<Edge> outgoing = curr.getOutgoing();
            if (outgoing.size() > 0) {
                for (Edge e : outgoing) {
                    q.add(e.getTo());
                }
            }
        }
        q = null;
    }
    /** Insert a Node into the tree
        @param from the parent Node
        @param to the new Node
        @param label the String label to give the new Node
    */
    public void insertChild(Node from, Node to, String label) {
        Edge e = new Edge(from, to, label);
        from.addOutgoing(e);
        to.addIncoming(e);
        this.size += 1;
    }
    /** Traverse the decisiontree using a String variable to guide
        its next decision.
        @param hidden the String variable to be used to guide
        @return String the class label
    */
    public String traverseTree(String hidden) {
        String result = this.findClassLabel(this.root, hidden.split(","), 0);
        return result;
    }
    private String findClassLabel(Node curr, String[] hiddenVector, int depth) {
          // curr.isClass() == true indicates that the node contains a class
          // label
          Random rand = new Random();
          while (curr.isClass() != true) {
            String nodeLabel = curr.getLabel();
            // labels will contain which attribute is up next
            int intLabel = Integer.parseInt(nodeLabel);
            ArrayList<Edge> edges = curr.getOutgoing();

            boolean found = false;
            for (Edge e : edges) {
                    // try-catch useful for checking feasibility in genetic
                    // algorithms as it points out logical flaws in the tree
                    try {
                    // if edge label and test attribute match up, then yay
                    if (hiddenVector[intLabel].compareTo(e.getLabel()) == 0) {
                        depth += 1;
                        // chop test vector 
                        hiddenVector = reduceHiddenVector(hiddenVector, intLabel);
                        // node is where the edge points to
                        curr = e.getTo();
                        found = true;
                        break;
                    }
                    } catch (ArrayIndexOutOfBoundsException ex) {
                        return null;
                    }
            }
            if (!found) {
                return mostProminent(curr.returnData());
                //int size = edges.size();
                //int randy = rand.nextInt(size);
                //curr = edges.get(randy).getTo();
            }
        }
        rand = null;
        return curr.getLabel();
    }
    private String mostProminent(ArrayList<String> data) {
        int ones = 0;
        int zero = 0;
        for (String str : data) {
            String[] split = str.split(",");
            String classLabel = split[0];
            if (classLabel.compareTo("1") == 0) {
                ones += 1;
            } else {
                zero += 1;
            }
        }
        if (ones >= zero) {
            return "1";
        }
        return "0";
    }
    /** Remove a string from a vector
        @param hiddenVector the test data vector
        @param intLabel the position to remove
        @return String[] the new vector
    */
    public String[] reduceHiddenVector(String[] hiddenVector, int intLabel) {
        // remove the position in the vector decided by intLabel and 
        // reform vector with one less dimension
        String newVector = "";
        for (int i = 0; i < hiddenVector.length; i++) {
            if (i != intLabel) {
                newVector += hiddenVector[i] + ",";
            }
        }
        if (newVector.length() > 0) {
            newVector = newVector.substring(0, newVector.length() - 1);
        }
        return newVector.split(",");
    }
    /** Deep copy a DecisionTree
        @param parent the tree to copy
        @return DecisionTree, a deep copy version of parent
    */
    public DecisionTree deepCopy(DecisionTree parent) {
         try {
             ObjectOutputStream oos = null;
             ObjectInputStream ois = null;
             DecisionTree p = null;
             ByteArrayOutputStream bos = new ByteArrayOutputStream();
             oos = new ObjectOutputStream(bos);
             oos.writeObject(parent);
             oos.flush();
             ByteArrayInputStream bin = new ByteArrayInputStream(bos.toByteArray    ());
             ois = new ObjectInputStream(bin);
             Object o = ois.readObject();
             p = (DecisionTree) o;
             oos.close();
             ois.close();
             return p;
         } catch (ClassNotFoundException | IOException e) {
             System.err.println("COULD NOT DEEP COPY");
         }
         return null;
     }
     
}
