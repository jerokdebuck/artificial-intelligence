
package edu.jhu.darian.cs335.hw4.DataStructures;

import java.io.*;
import java.util.Set;
import java.util.HashSet;

/**
    A class that holds (Entropy, Position) values
*/

public class EntropyVectorTuple implements Serializable {

    private static final long serialVersionUID = 7526472295622776147L;

    private double entropy;
    private int vector_position;
    private Set<String> classLabels;
    /** Create EntropyVectorTuple instance
        @param entropy the entropy to be input
        @param vector_position the next decision location
    */

    public EntropyVectorTuple(double entropy, int vector_position) {
        this.entropy = entropy;
        this.vector_position = vector_position;
        this.classLabels = new HashSet<String>();
    }
    /** Input a set of classLabels
        @param classLabels the set of classLabels
    */
    public EntropyVectorTuple(Set<String> classLabels) {
        this.classLabels = classLabels;
    }
    /** Return the entropy of the system
        @return double the entropy
    */
    public double getEntropy() {
        return this.entropy;
    }
    /** Return the vector_position
        @return int the vector position
    */
    public int getPosition() {
        return this.vector_position;
    }
    /**
        Return classLabels
        @return Set the classlabels
    */
    public Set<String> returnLabels() {
        return this.classLabels;
    }


}
