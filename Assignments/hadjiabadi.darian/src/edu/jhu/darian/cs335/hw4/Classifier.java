package edu.jhu.darian.cs335.hw4;
import edu.jhu.darian.cs335.hw4.DataStructures.EntropyVectorTuple;
import edu.jhu.darian.cs335.hw4.DataStructures.DecisionTree;
import edu.jhu.darian.cs335.hw4.DataStructures.Node;
import edu.jhu.darian.cs335.hw4.DataProcessor;

import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.lang.Math;
import java.io.*;

/**
    Classifier class allows building of decision tree on trainingData and
    testing of decisionTree on testing data
*/
    
public class Classifier { 

    private int vector_length;
    private ArrayList<String> trainingData;
    private ArrayList<String> testData;

    private Set<String> classLabels;
    private DataProcessor dp;
    private boolean useIG;

    /**
        Creates Classifier instance.
        @param trainingData the input training list
        @param vector_length the length of the attribute vector
        information gain or gain ratio
    */
    public Classifier(ArrayList<String> trainingData, int vector_length, boolean useIG) {
        this.trainingData = trainingData;
        this.vector_length = vector_length;
        this.useIG = useIG;
    }
    /** Creates a classifier instance
    */
    public Classifier() {
    }
    public DecisionTree train() {
        // process data and get maximum (entropy,position) back
        dp = new DataProcessor(this.trainingData, this.vector_length, this.useIG);
        EntropyVectorTuple max = dp.process();
        ArrayList<Set<String>> attributePossibilities = dp.getPossibilities();
        String nodeLabel = Integer.toString(max.getPosition());
        // initialize DecisionTree with root data containing data
        // that was retrieved from DataProcessor
        DecisionTree dTree = new DecisionTree();
        Node root = new Node(nodeLabel, this.trainingData, attributePossibilities, max, vector_length);
        dTree.insertRoot(root);
        // start the building
        this.buildDecisionTree(dTree, dTree.getRoot());
        //this.printTree(dTree);

        return dTree;
    }
    /**
        A recursive function used to build a decision tree
        @param dTree the DecisionTree to build
        @param curr, the current Node
    */
    private void buildDecisionTree(DecisionTree dTree, Node curr) {
        // get the EntropyVectorTuple instance from the current node
         EntropyVectorTuple max = curr.returnMax();
        // retrieve all possible decisions
         String[] attributeLabels = new String[curr.returnAttPossibilities().get(max.getPosition()).size()];
         curr.returnAttPossibilities().get(max.getPosition()).toArray(attributeLabels);
          Node q;
          for (int i = 0; i < attributeLabels.length; i++) {
              // Deep copy the node and alter its data based on 
              // the attribute label
              // Deep copying necessary to preserve parent node's data
              // when it comes to making multiple decisions
              Node childCopy = curr.deepCopy(curr);
              alterData(childCopy, attributeLabels[i]);
              // process the concatonated data to obtain new 
              // decisions
              dp = new DataProcessor(childCopy.returnData(), childCopy.getLength() - 1, this.useIG);
              // the new maximum (entropy, position) vector
              EntropyVectorTuple newMax = dp.process();
              Set<String> classLabels = newMax.returnLabels();
              // if there is only one class label found in the
              // concatonated data, then it must be the case
              // the decision only leads to one possible outcome
              if (classLabels.size() == 1) {

                  String finalLabel = "";
                  for(String label : classLabels) {
                      finalLabel = label;
                  }
                  // get the label fom the set and make a new node
                  // containing the class label
                  q = new Node(finalLabel);
                  dTree.insertChild(curr, q, attributeLabels[i]);
              } else {
                  // if not then get the possibilities and make a new node
                  ArrayList<Set<String>> attributePossibilities = dp.getPossibilities();
                  // new node will contain the next vector position
                  // as well as the concatenated data, attributes,
                  // the max (entropy, position) tuple and the new size
                  // of the vector length (which should be one less)
                  q = new Node(Integer.toString(newMax.getPosition()), childCopy.returnData(), attributePossibilities, newMax, childCopy.getLength() - 1);
                  // insert and keep building
                  dTree.insertChild(curr, q, attributeLabels[i]);
                  this.buildDecisionTree(dTree, q);
              }
          } 
    }
    /**
        Filters out a list of data to take into account
        the attribute label and position of the label.
        @param n, the Node with which to filter out data from
        @param label, the attribute label with which to find
    */
    private void alterData(Node n, String label) {
        ArrayList<String> data = n.returnData();
        EntropyVectorTuple max = n.returnMax();
        int position = max.getPosition();
        // find all data lines matching the position and label
        ArrayList<String> newData = new ArrayList<String>();
        for (String str: data) {
            String[] split = str.split(",");
            // if the vector at proper position matches the label
            // add it to the new data expect at the position point
            if (split[position + 1].compareTo(label) == 0) {
                String newLine = "";
                for (int i = 0; i < split.length; i++) {
                    if (i != position + 1) {
                        newLine += split[i] + ",";
                    }
                }
                newLine = newLine.substring(0, newLine.length() - 1);
                newData.add(newLine);
              }
          }
          n.insertData(newData);
    }
    public int[] test(ArrayList<String> testData, DecisionTree dTree) {
        // make sure a proper decision tree is given
        if (dTree == null) {
            throw new IllegalArgumentException("Null decision tree");
        }
        // chop test data into classes and hidden lists
        ArrayList<String> hidden = new ArrayList<String>();
        ArrayList<String> classes = new ArrayList<String>();
        this.testData = testData;

        for (String str : this.testData) {
            String[] split = str.split(",");
            // position 0 has class labels, add that to classes
            classes.add(split[0]);
            String newLine = "";
            // add rest to hidden
            for (int i = 1; i < split.length; i++) {
                newLine += split[i] + ",";
            }
            newLine = newLine.substring(0, newLine.length() - 1);
            hidden.add(newLine);
        }
        int total;
        int true_positive = 0;
        int true_negative = 0;
        int false_positive = 0;
        int false_negative = 0;
        for (total = 0; total < hidden.size(); total++) {
            // find the result of running the hidden data vector
            // through the tree
            String results = dTree.traverseTree(hidden.get(total));
            if (results == null) {
                // if the result is null, then we got bad data
                int[] badReturn = {0,0,0,0};
                return badReturn;
            }
            if (results.compareTo(classes.get(total)) == -1) {
                false_negative += 1;
            } else if (results.compareTo(classes.get(total)) == 1) {
                false_positive += 1;
            } else if (results.compareTo(classes.get(total)) == 0 && results.compareTo("0") == 0) {
                true_negative += 1;
            } else {
                true_positive += 1;
            }
         }
         int[] results = {true_positive, true_negative, false_positive, false_negative};
         return results;
    }
}
