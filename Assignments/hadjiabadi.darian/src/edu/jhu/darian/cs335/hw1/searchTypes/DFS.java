// Darian Hadjiabadi
// dhadjia1@gmail.com
// dhadjia1

package edu.jhu.darian.cs335.hw1.searchTypes;
import edu.jhu.darian.cs335.hw1.helperNodes.UninformedNode;
import edu.jhu.darian.cs335.hw1.helperGraphs.UninformedGraph;
import edu.jhu.darian.cs335.hw1.helperProcessing.FileInformation;
import java.util.Stack;
import java.util.ArrayList;

/**
    Allows user to run a DFS search given appropriate FileInformation object
*/

public class DFS {
    private FileInformation fileInfo = null;
    /**
        Creates DFS instance
        @param fileInfo the FileInformation object containing all 
        appropriate map data
    */
    public DFS(FileInformation fileInfo) {
        this.fileInfo = fileInfo;
    }
    /**
        Perform DFS search
        @throws IllegalArgumentException if fileInfo is null
    */
    public void Search() {
        if (this.fileInfo == null) {
            throw new IllegalArgumentException();
        }
        int[] startInfo = this.fileInfo.getStartInfo();
        int[] dimensions = this.fileInfo.getStateDimensions();
        char[][] cells = this.fileInfo.getCell();
        UninformedGraph graph = new UninformedGraph(startInfo, dimensions, cells);
        this.search(graph);
       
    }    
    private void search(UninformedGraph graph) {
        Stack<UninformedNode> s = new Stack<UninformedNode>();
        UninformedNode root = graph.getStart();
        s.push(root);
        int expanded = 0;
        while (!s.empty()) {
            UninformedNode n = s.pop();
            if (!graph.isDiscovered(n)) {
                expanded += 1;
                graph.discovered(n);
                ArrayList<UninformedNode> list = graph.adjacents(n);
                for (UninformedNode q : list) {
                     if (q != null) {
                         if (q.getType() == 'g') {
                             this.printSolution(q, expanded);
                             return;
                         }
                         s.push(q);
                     }
                 }
             }
         }
        System.out.println("No solution found, cost = infinity!");
    }
    private void printSolution(UninformedNode n, int expanded) {
        Stack<UninformedNode> s = new Stack<UninformedNode>();
        int count = -1;
        while (n != null) {
            count += n.getStepCount();
            s.push(n);
            n = n.getParent();
        }
        System.out.print("Start -> ");
        while (!s.empty()) {
            UninformedNode q = s.pop();
            if (q.getType() == 'g') {
                System.out.print("Goal -> ");
            }
            System.out.println("(" + q.getRow() + ", " + q.getCol() + ")");
        }
        System.out.println("Total cost of: " + count);
        System.out.println("Expanded: " + expanded + " nodes");
    }
} 
