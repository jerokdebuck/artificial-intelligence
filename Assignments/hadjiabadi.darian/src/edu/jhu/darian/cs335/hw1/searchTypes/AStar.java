// Darian Hadjiabadi
// dhadjia1@gmail.com
// dhadjia1

package edu.jhu.darian.cs335.hw1.searchTypes;
import edu.jhu.darian.cs335.hw1.helperProcessing.FileInformation;
import edu.jhu.darian.cs335.hw1.helperNodes.InformedNode;
import edu.jhu.darian.cs335.hw1.helperGraphs.InformedGraph;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Comparator;
import java.util.Stack;
/**
    Allows uer to run an AStar search given the appropriate FileInformation object
*/
public class AStar {
    private FileInformation fileInfo = null;
    private int mapNum;
    /**
        Creates an AStar instance
        @param fileInfo the FileInformation object containing map data
        @param mapNum the map number that will be searched
    */
    public AStar(FileInformation fileInfo, int mapNum) {
        this.fileInfo = fileInfo;
        this.mapNum = mapNum;
    }
    /**
        Performs the AStar search with appropriate heuristics
        @throws IllegalArgumentException if fileInfo is null
    */
    public void Search() {
        if (this.fileInfo == null) {
            throw new IllegalArgumentException();
        }
        int[] startInfo = this.fileInfo.getStartInfo();
        int[] goalInfo = this.fileInfo.getGoalInfo();
        int[] dimensions = this.fileInfo.getStateDimensions();
        char[][] cells = this.fileInfo.getCell();
        InformedGraph graph = new InformedGraph(startInfo, goalInfo, dimensions, cells, this.mapNum);
        this.search(graph);
    }
    private void search(InformedGraph graph) {
        // PriorityQueue allows ordering
        Comparator<InformedNode> comparator = new EstimationComparator();
        PriorityQueue<InformedNode> queue = new PriorityQueue<InformedNode>(20, comparator);
        InformedNode root = graph.getStart();
        queue.add(root);
        int expanded = 0;
        while (queue.peek() != null) {
            InformedNode n = queue.poll();
            if (!graph.isDiscovered(n)) {
                expanded += 1;
                graph.discovered(n);
                if (n.getType() == 'g') {
                    this.printSolution(n, n.getCostToNode(), expanded);
                    return;
                }
                ArrayList<InformedNode> list = graph.adjacents(n);
                for (InformedNode q : list) {
                    if (q != null) {
                        queue.add(q);
                    }
                }
            }
        }
        System.out.println("No Solution found: cost = infinity");
    }
    private void printSolution(InformedNode n, int count, int expanded) {
        Stack<InformedNode> s = new Stack<InformedNode>();
        while (n != null) {
            s.push(n);
            n = n.getParent();
        }
        System.out.print("Start -> ");
        while (!s.empty()) {
            InformedNode q = s.pop();
            if (q.getType() == 'g') {
                System.out.print("Goal -> ");
            }
            System.out.println("(" + q.getRow() + ", " + q.getCol() + ")");
        }
        System.out.println("Total cost of: " + count);
        System.out.println("Expanded: " + expanded + " nodes");
    }
    // Allows ordering to be based on specified comparison
    private class EstimationComparator implements Comparator<InformedNode> {
        //@Override
        public int compare(InformedNode n1, InformedNode n2) {
            if (n1.getEstimatedSolution() - n2.getEstimatedSolution() > 0) {
                return 1;
            } else if (n1.getEstimatedSolution() - n2.getEstimatedSolution() > 0) {
                return -1;
            }
            return 0;
        }
    }
}
