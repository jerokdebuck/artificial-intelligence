// Darian Hadjiabadi
// dhadjia1@gmail.com
// dhadjia1

package edu.jhu.darian.cs335.hw1.helperNodes;
/**
    Node class that contains information relevant for an 
    informed search.
*/
public class InformedNode {
    private InformedNode parent;
    private char type;
    private int row;
    private int col;
    private int stepCount;
    private int costToNode;
    private int costToGoal;
    /**
        Creates InformedNode instance
        @param type the character type associated with the node
        @param row the row the node belongs to
        @param col the column the node belongs to
        @param parent the parent of the node
        @param costToNode the current cost from start position to this node
        @param costToGoal cost form current node to the goal node
    */
    public InformedNode(char type, int row, int col, InformedNode parent, int costToNode, int costToGoal) {
        this.parent = parent;
        this.type = type;
        this.row = row; 
        this.col = col;
        this.stepCount = this.getCount();
        this.costToNode = costToNode;
        this.costToGoal = costToGoal;
    }
    /**
        Returns the estimated solution, f(n)
        @return int the value of the estimated solution
    */
    public double getEstimatedSolution() {
        // f(n) = g(n) + h(n)
        return this.costToNode + this.costToGoal;
    }
    /**
        Returns the row of the node
        @return int the row of the node
    */
    public int getRow() {
        return this.row;
    }
    /**
        Returns the column of the node
        @return int the column of the node
    */
    public int getCol() {
        return this.col;
    }
    /**
        Returns the char type of the node
        @return char the type of the node
    */
    public char getType() {
        return this.type;
    }
    /**
        Returns the node's parent
        @return InformedNode the node's parent
    */
    public InformedNode getParent() {
        return this.parent;
    }
    private int getCount() {
        int val = 0;
        if (this.type == '.' || this.type == 's' || this.type == 'g') {
            val = 1;
        } else if (this.type == ',') {
            val = 2;
        }
        return val;
    }
    /**
        Return g(n), the cost from start node to current node
        @return int the cost to the node from the start node
    */
    public int getCostToNode() {
        return this.costToNode;
    }
}
