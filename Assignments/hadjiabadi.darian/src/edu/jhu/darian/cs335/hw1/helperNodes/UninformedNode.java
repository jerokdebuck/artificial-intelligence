// Darian Hadjiabadi
// dhadjia1@gmail.com
// dhadjia1

package edu.jhu.darian.cs335.hw1.helperNodes;
/**
    Node class appropriate for Uninformed searches
*/
public class UninformedNode {
    private UninformedNode parent;
    private char type;
    private int row;
    private int col;
    private int stepCount;
    /**
        Creates UninformedNode instance
        @param type the character associated with the node
        @param row the row the node belongs to
        @param col the col the node belongs to
        @param parent the parent of the node
    */
    public UninformedNode(char type, int row, int col, UninformedNode parent) {
        this.parent = parent;
        this.type = type;
        this.row = row;
        this.col = col;
        this.stepCount = this.getCount();
    }
    /**
        Returns the row of the node
        @return int the row of the node
    */
    public int getRow() {
        return this.row;
    }
    /**
        Returns the column of the node
        @return int the column of the node
    */
    public int getCol() {
        return this.col;
    }
    /**
        Returns the char type of the node
        @return char the character associated with the node
    */
    public char getType() {
        return this.type;
    }
    /**
        Returns the parent of the node
        @return UninformedNode the parent of the node
    */
    public UninformedNode getParent() {
        return this.parent;
    }
    private int getCount() {
        int val = 0;
        if (this.type == '.' || this.type == 's' || this.type == 'g') {
            val = 1;
        } else if (this.type == ',') {
            val = 2;
        }
        return val;
    }
    /**
        Returns the step count of the node
        @return int the step count to the node
    */
    public int getStepCount() {
        return this.stepCount;
    }
}
