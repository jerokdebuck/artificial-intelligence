// Darian Hadjiabadi
// dhadjia1@gmail.com
// dhadjia1
package edu.jhu.darian.cs335.hw1.helperProcessing;

import edu.jhu.darian.cs335.hw1.helperProcessing.FileInformation;
import edu.jhu.darian.cs335.hw1.searchTypes.AStar;
import edu.jhu.darian.cs335.hw1.searchTypes.BFS;
import edu.jhu.darian.cs335.hw1.searchTypes.DFS;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
    SearchRunner class processes map text files, retreiving 
    appropriate information and putting such information into
    a FileInformation object.

    SearchRunner will also run the appropriate search choice based
    off of the user input
*/
public class SearchRunner {

    private String searchChoice;
    private int mapNum;
    private FileInformation fileInfo = null;
    /** Creates SearchRunner instance
        @param searchChoice the type of search to run
        @param mapNum the map to use
    */
    public SearchRunner(String searchChoice, int mapNum) {
        this.searchChoice = searchChoice;
        this.mapNum = mapNum;
    }
    /** Processes the appropriate map and puts information into
        FileInformation object
        @param directory the directory of the input data
        @return FileInformation object created
        @throws IllegalArgumentException if the file does not exist
    */
    public void textProcess(String directory) throws FileNotFoundException, IOException {
        String name = "map" + this.mapNum + ".txt";
        File f = new File(directory + name);
        if (!f.exists()) {
            throw new IllegalArgumentException("Error: data does not exist");
        }
        this.fileInfo = this.readFile(new File(directory + name));
    }
    private FileInformation readFile(File f) throws IOException, FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader(f));
        String line;
        String[] lineSplit;

        line = br.readLine();
        lineSplit = line.split(" ");
        int stateLength = Integer.parseInt(lineSplit[1]);
        int stateWidth = Integer.parseInt(lineSplit[0]);
        int row = 0;
        int startRow = -1;
        int startCol = -1;
        int goalRow = -1;
        int goalCol = -1;
        StringBuilder sb = new StringBuilder();
        String[] charSplit;
        while ((line = br.readLine()) != null) {
            charSplit = line.split("");
            sb.append(line);
            for (int col = 0; col < charSplit.length; col++) {
                if (charSplit[col].compareTo("s") == 0) {
                    startRow = row;
                    startCol = col - 1;
                } else if (charSplit[col].compareTo("g") == 0) {
                    goalRow = row;
                    goalCol = col - 1;
                }
            }
            row++;
        }
        return new FileInformation(sb.toString(), startRow, startCol, goalRow, goalCol, stateLength, stateWidth);
    }
    /** Runs the search
        @param f the object containing all necessary information
                  to run the search.
        @throws IllegalArgumentException if the FileInformation object is null
    */
    public void runSearch() {
        if (this.fileInfo == null) {
            throw new IllegalArgumentException("Error, file information has null value");
        }
        if (this.searchChoice.compareTo("BFS") == 0) {
            this.BreadthFirstSearch(this.fileInfo);
        } else if (this.searchChoice.compareTo("DFS") == 0) {
            this.DepthFirstSearch(this.fileInfo);
        } else {
            this.AStarSearch(this.fileInfo);
        }
    }
    private void BreadthFirstSearch(FileInformation f) {
        BFS bfs = new BFS(f);
        bfs.Search();
    }
    private void DepthFirstSearch(FileInformation f) {
        DFS dfs = new DFS(f);
        dfs.Search();
    }
    private void AStarSearch(FileInformation f) {
        AStar astar = new AStar(f, this.mapNum);
        astar.Search();
    }
}
    
