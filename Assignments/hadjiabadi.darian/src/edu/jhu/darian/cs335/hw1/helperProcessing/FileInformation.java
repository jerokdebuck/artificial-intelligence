// Darian Hadjiabadi
// dhadjia1@gmail.com
// dhadjia1

package edu.jhu.darian.cs335.hw1.helperProcessing;
/** FileInformation class contains information about the map to be
    searched.
    
    Contains information such as the full map directory,
    the starting row, starting column, goal row, goal column,
    state space length and statespace width.
*/
public class FileInformation {
    private String mapString;
    private int startRow;
    private int startCol;
    private int goalRow;
    private int goalCol;
    private int stateLength;
    private int stateWidth;
    /**
        Creates a FileInformation instance
        @param mapString the directory + name of the map to be searched
        @param startRow the row of the starting node
        @param startCol the column of the starting node
        @param goalRow the row of the goal node
        @param goalCol the column of the goal node
        @param stateLength the length of the state space
        @param stateWidth the width of the state space
    */
    public FileInformation(String mapString, int startRow, int startCol,     int goalRow, int goalCol, int stateLength, int stateWidth) {
        this.mapString = mapString;
        this.startRow = startRow;
        this.startCol = startCol;
        this.goalRow = goalRow;
        this.goalCol = goalCol;
        this.stateLength = stateLength;
        this.stateWidth = stateWidth;
    }
    /**
        Returns the text based map directory.
        @return String the string of the directory
    */
    public String getMapString() {
        return this.mapString;
    }
    /**
        Returns the start node information.
        @return int[] the array containing starting information
    */
    public int[] getStartInfo() {
        int[] a = {this.startRow, this.startCol};
        return a;
    }
    /**
        Returns the goal node information
        @return int[] the array containing the goal information
    */
    public int[] getGoalInfo() {
        int[] a = {this.goalRow, this.goalCol};
        return a;
    }
    /**
        Gets the length and width of the state space
        @return int[] the array containing the state space information
    */
    public int[] getStateDimensions() {
        int[] a = {this.stateLength, this.stateWidth};
        return a;
    }
    /**
        Returns a 2D matrix of the statespace.
        @return char[][] the 2D matrix containing state space characters
    */
    public char[][] getCell() {
        char[][] cells = new char[stateLength][stateWidth];
        int count = 0;
        for (int i = 0; i < this.stateLength; i++) {
            for (int j = 0; j < this.stateWidth; j++) {
                cells[i][j] = this.mapString.charAt(count);
                count++;
            }
        }
        return cells;
    }    
}

