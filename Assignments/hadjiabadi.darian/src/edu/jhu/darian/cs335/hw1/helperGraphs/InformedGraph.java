// Darian Hadjiabadi
// dhadjia1@gmail.com
// dhadjia1
package edu.jhu.darian.cs335.hw1.helperGraphs;
import edu.jhu.darian.cs335.hw1.helperNodes.InformedNode;
import java.util.ArrayList;

/**
    Graph based class for informed searches
*/
public class InformedGraph {
    private int[] startInfo;
    private int[] goalInfo;
    private int rows;
    private int cols;
    private int mapNum;
    private char[][] cells;
    private boolean[][] discovered;
    /**
        Creates InformedGraph instance
        @param startInfo the array containing information of the starting node
        @param goalInfo the array containing information of the goal node
        @param dimensions the array containing the dimensions of the state space
        @param cells the 2D matrix containing the state space
        @param mapNum the map number to be run
    */
    public InformedGraph(int[] startInfo, int[] goalInfo, int[] dimensions, char[][] cells, int mapNum) {
        this.startInfo = startInfo;
        this.goalInfo = goalInfo;
        this.rows = dimensions[0];
        this.cols = dimensions[1];
        this.cells = cells;
        this.mapNum = mapNum;
        this.discovered = new boolean[this.rows][this.cols];
    }
    /**
        Returns true if node has been discovered, false otherwise
        @param n the InformedNode to be checked
        @return boolean true if discovered, false otherwise
    */
    public boolean isDiscovered(InformedNode n) {
        int row = n.getRow();
        int col = n.getCol();
        return this.discovered[row][col];
    }
    /**
        Changes a node to be discovered
        @param n the InformedNode that is now discovered
    */    
    public void discovered(InformedNode n) {
        int row = n.getRow();
        int col = n.getCol();
        this.discovered[row][col] = true;
    }
    /**
        Returns the number of vertices associated with the class
        @return int the number of vertices
    */
    public int numVertices() {
        return this.rows * this.cols;
    }
    /**
        Returns an InformedNode object that describes the starting node
        @return InformedNode the starting node
    */
    public InformedNode getStart() {
        int costToGoal = this.getCostToGoal(startInfo[0], startInfo[1]);
        return new InformedNode('s', startInfo[0], startInfo[1], null, 0, costToGoal);
    }
    /**
        Returns the adjacent nodes of a given input InformedNode.
        @param parent the node whose neighbors need to be found
        @return ArrayList the list containing the neighbors
    */
    public ArrayList<InformedNode> adjacents(InformedNode parent) {
        int row = parent.getRow();
        int col = parent.getCol();
        // Not all nodes will have four neighbors, check to make sure
        // they are in appropriate positions as assigning a neighbor
        // at a particular position may result in an exception
        ArrayList<InformedNode> list = new ArrayList<InformedNode>();
        if (row + 1 < this.rows) {
            InformedNode n = this.get(row + 1, col, parent);
            list.add(n);
        }
        if (row - 1 >= 0) {
            InformedNode n = this.get(row - 1, col, parent);
            list.add(n);
        }
        if (col + 1 < this.cols) {
            InformedNode n = this.get(row, col + 1, parent);
            list.add(n);
        }
        if (col - 1 >= 0) {
            InformedNode n = this.get(row, col - 1, parent);
            list.add(n);
        }
        return list;
    }
    /*
        Maps 1,2, & 4 use manahatten distance for its heuristic.
        Maps 3,5,6,7, & 8 use the euclidean distance
    */
    private int getCostToGoal(int row, int col) {
        int xdist = this.abs(row - this.goalInfo[0]);
        int ydist = this.abs(row - this.goalInfo[1]);
        int euclid = (int) Math.sqrt(xdist*xdist + ydist*ydist); 
        if (this.mapNum < 3 || this.mapNum == 4) {
            return xdist + ydist;
        } else {
            return (int) Math.sqrt(xdist*xdist + ydist*ydist);
        }
    }
    private int abs(int val) {
        if (val < 0) {
            return -1 * val;
        }
        return val;
    }
    private InformedNode get(int row, int col, InformedNode parent) {
        
        int costToParent = parent.getCostToNode();
        int stepCount;
        int costToGoal;
        // costToParent = g(n) - stepCount
        // costToGoal = h(n)
        if (this.cells[row][col] == '.') {
            stepCount = 1;
            costToGoal = this.getCostToGoal(row, col); // use heuristic here
            return new InformedNode('.', row, col, parent, stepCount + costToParent, costToGoal);
        } else if (this.cells[row][col] == ',') {
            stepCount = 2;
            costToGoal = this.getCostToGoal(row, col);
            return new InformedNode(',', row, col, parent, stepCount + costToParent, costToGoal);
        } else if (this.cells[row][col] == 's') {
            stepCount = 1;
            costToGoal = this.getCostToGoal(row, col);
            return new InformedNode('s', row, col, parent, stepCount + costToParent, costToGoal);
        } else if (this.cells[row][col] == 'g') {
            stepCount = 1;
            costToGoal = this.getCostToGoal(row, col);
            return new InformedNode('g', row, col, parent, stepCount + costToParent, costToGoal);
        }
        return null; // if #
    }
}
