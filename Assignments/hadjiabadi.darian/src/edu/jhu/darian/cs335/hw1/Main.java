// Darian Hadjiabadi
// dhadjia1@gmail.com
// dhadjia1
package edu.jhu.darian.cs335.hw1;
import edu.jhu.darian.cs335.hw1.helperProcessing.SearchRunner;
import edu.jhu.darian.cs335.hw1.helperProcessing.FileInformation;

import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
   Artificial Intelligence Assignment 1 : Search
*/
public final class Main {
    private Main() {}
    // Allows uer to input through command line
    static String checkArg(String input) {
        check(input);
        return input;
    }
    // Input must be 'BFS', 'DFS', or 'AStar'. Any other input
    // will throw an exception
    static void check(String input) {
        if (input.compareTo("BFS") != 0 && input.compareTo("DFS") != 0 && input.compareTo("AStar") != 0) {
            throw new IllegalArgumentException("Error, wrong search choice");
        }
    }
    /**
        Main method.
        @param args Command linea rguments
        @throws FileNotFoundException If text map cannot be opened
        @throws IOException if database file cannot be read properly
    */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        String searchChoice;
        int mapNum;
        searchChoice = checkArg(args[0]);
        mapNum = Integer.parseInt(args[1]);
        SearchRunner sr = new SearchRunner(searchChoice, mapNum);
        sr.textProcess("../data/");
        try {
            sr.runSearch();
        } catch (Error e) {
            System.out.println("Error: Out of heap space. Solution for map" + mapNum + ".txt could not be found. Cost = infinity!");
        }
    }
}
