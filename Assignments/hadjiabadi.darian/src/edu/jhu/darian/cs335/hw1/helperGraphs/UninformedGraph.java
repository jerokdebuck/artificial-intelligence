// Darian Hadjiabadi
// dhadjia1@gmail.com
// dhadjia1

package edu.jhu.darian.cs335.hw1.helperGraphs;
import edu.jhu.darian.cs335.hw1.helperNodes.UninformedNode;
import java.util.ArrayList;

/**
    Graph based class appropriate for uninformed searches
*/
public class UninformedGraph {
    private int[] startInfo;
    private int rows;
    private int cols;
    private char[][] cells;
    private boolean[][] discovered;
    /**
        Creates UninformedGraph instance
        @param startInfo the array containing starting node row + col
        @param dimensions the length and width of the state space
        @param cells the 2D matrix containing the state space
    */
    public UninformedGraph(int[] startInfo, int[] dimensions, char[][] cells) {
        this.startInfo = startInfo;
        this.rows = dimensions[0];
        this.cols = dimensions[1];
        this.cells = cells;
        this.discovered = new boolean[this.rows][this.cols];
    }
    /**
        Return true if node discovered, false otherwise
        @param n the UninformedNode to check
        @return boolean true if discovered, false otherwise
    */
    public boolean isDiscovered(UninformedNode n) {
        int row = n.getRow();
        int col = n.getCol();
        return this.discovered[row][col];
    }
    /**
        Makes the node passed as argument to be discovered
        @param n the UninformedNode that will be discovered
    */
    public void discovered(UninformedNode n) {
        int row = n.getRow();
        int col = n.getCol();
        this.discovered[row][col] = true;
    }
    /**
        Return the number of vertcies in the instance
        @return int the number of vertices
    */
    public int numVertices() {
        return this.rows * this.cols;
    }
    /**
        Returns an UninformedNode containing information on the start node
        @return UninformedNode the start node
    */
    public UninformedNode getStart() {
        return new UninformedNode('s', startInfo[0], startInfo[1], null);
    }
    /**
        Gets neighbors of a node
        @param parent the node with which to get neighbors from
        @return ArrayList a list containing all of its neighbors
    */
    public ArrayList<UninformedNode> adjacents(UninformedNode parent) {
        int row = parent.getRow();
        int col = parent.getCol();
        ArrayList<UninformedNode> list = new ArrayList<UninformedNode>();
        // Not every node has 4 neighbors, check to make sure
        // that particular row + col patterns exist
        if (row + 1 < this.rows) {
            UninformedNode n = this.get(row + 1, col,parent);
            list.add(n);
        }
        if (row - 1 >= 0) {
            UninformedNode n = this.get(row - 1, col,parent);
            list.add(n);
        }
        if (col + 1 < this.cols) {
            UninformedNode n = this.get(row, col + 1, parent);
            list.add(n);
        }
        if (col - 1 >= 0) {
            UninformedNode n = this.get(row, col - 1, parent);
            list.add(n);
        }
        return list;
    }
    private UninformedNode get(int row, int col, UninformedNode parent) {
        if (this.cells[row][col] == '.') {
            return new UninformedNode('.', row, col, parent);
        } else if (this.cells[row][col] == ',') {
            return new UninformedNode(',', row, col, parent);
        } else if (this.cells[row][col] == 's') {
            return new UninformedNode('s', row, col, parent);
        } else if (this.cells[row][col] == 'g') {
            return new UninformedNode('g', row, col, parent);
        }
        return null;
    }
}
