// Darian Hadjiabadi
// dhadjia1@gmail.com
// dhadjia1

package edu.jhu.darian.cs335.hw1.searchTypes;
import edu.jhu.darian.cs335.hw1.helperGraphs.UninformedGraph;
import edu.jhu.darian.cs335.hw1.helperNodes.UninformedNode;
import edu.jhu.darian.cs335.hw1.helperProcessing.FileInformation;
import java.util.Queue;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Stack;
/**
    Allows uer to run a BFS search given appropriate FileInformation object
*/
public class BFS {
    private FileInformation fileInfo = null;
    
    /**
        Creates BFS instance
        @param fileInfo the FileInformation object containing all 
        appropriate map info
    */
    public BFS(FileInformation fileInfo) {
        this.fileInfo = fileInfo;
    }
    /**
        Runs BFS search
        @throws IllegalArgumentException if fileInfo is null
    */
    public void Search() {
        if (this.fileInfo == null) {
            throw new IllegalArgumentException();
        }
        int[] startInfo = this.fileInfo.getStartInfo();
        int[] dimensions = this.fileInfo.getStateDimensions();
        char[][] cells = this.fileInfo.getCell();
        
        UninformedGraph graph = new UninformedGraph(startInfo, dimensions, cells);
        this.search(graph);
    }
    private void search(UninformedGraph graph) {
        Queue<UninformedNode> que = new LinkedList<UninformedNode>(); 
        UninformedNode root = graph.getStart();
        que.add(root);
        graph.discovered(root);
        int expanded = 0;
        while (que.peek() != null) {
            UninformedNode n = que.poll();
            expanded += 1;
            graph.discovered(n);
            ArrayList<UninformedNode> list = graph.adjacents(n);
            for (UninformedNode q : list) {
                if (q != null) {
                    if (q.getType() == 'g') {
                        this.printSolution(q, expanded);
                        return;
                    }
                    if (!graph.isDiscovered(q)) {
                        que.add(q);
                    }
                }
            }  
        }
        System.out.println("No solution, cost = infinity");
    }
    private void printSolution(UninformedNode n, int expanded) {
        Stack<UninformedNode> s = new Stack<UninformedNode>();
        int count = -1;
        while (n != null) {
            count += n.getStepCount();
            s.push(n);
            n = n.getParent();
        }
        System.out.print("Start -> ");
        while (!s.empty()) {
            UninformedNode q = s.pop();
            if (q.getType() == 'g') {
                System.out.print("Goal -> ");
            }
            System.out.println("(" + q.getRow() + ", " + q.getCol() + ")");
        }
        System.out.println("Total cost of: " + count);
        System.out.println("Expanded: " + expanded + " nodes");
    }
}
